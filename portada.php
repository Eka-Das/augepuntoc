
<!DOCTYPE html>
<html>
<head>
<title>Auge Punto Central</title>
<meta name="description" content="" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link rel="stylesheet" type="text/css" href="css/197.css"/>
<style>
@import url(https://fonts.googleapis.com/css?family=Open+Sans:400italic,400,700|Overlock);.galbk-bot-base.spread.prev, .galbk-bot-base.spread.next { background-color:#141414; background-color:rgba(20,20,20,0.4); }.galbk-bot-base.spread.prev:hover, .galbk-bot-base.spread.next:hover { background-color:rgba(20,20,20,0.7); opacity:1; }.card_g .titulo.banda { background-color:#141414; background-color:rgba(20,20,20,0.5); }.wrapper-portada, .bottom-portada .linksEnter { background: #141414}body {color:#BABABA;font-family:Open Sans,sans-serif;font-size:13px;background:#FFF ;background-color:#141414;padding:5px 0 0 0; margin:0;}#container {margin:0 auto;padding:18px;}#top {width:100%;position:relative;height:100px;z-index:11;border-bottom:1px solid rgba(125,125,125,0.2);}.inGal #top, .bodyBKblog #top { border-bottom:1px solid transparent; }.bodyBKblog #top { border-bottom:1px solid transparent; }#logo {position:absolute;bottom:8px;left:0;width:250px;z-index:11;}#logo h1 { margin:0 }#logo h1 a {display:block;height:100px;width:100%;text-indent:-2000em;text-decoration:none;z-index:1000;}#menu {position:absolute;bottom:8px;margin:0;padding:0;z-index:12;right:0;left:270px;}#menu ul {text-align:right;list-style:none;margin:0; padding:0;}#menu li {position:relative;display:inline-block;padding:0;margin:0;}#menu a {display:block;padding:5px 5px 10px 0;padding-left:15px;color:#F0F0F0;font-family:Overlock,verdana;font-size:14px;}#menu ul.l2 {position:absolute;left:-20px; top:100%;margin:0;padding:15px 20px 20px;text-align:left;background:#FFF ;background-color:#141414;border-bottom:1px solid rgba(125,125,125,0.4);z-index:10;visibility:hidden;opacity:0;transition:visibility .2s, opacity 0.2s linear;}#menu li:hover ul.l2 {visibility:visible;opacity:1;}#menu ul.l2 li {margin:0;padding:0;width:150px;}#menu ul.l2 a {padding:7px 0;line-height:1.25em;padding-left:15px;}#menu a:hover, #menu a.current {color:#8C8C8C;}#middleWrapper {position:relative;}#middle {padding:0;position:relative;}#contents {min-height:250px;margin-top:30px;}.fotogrande #contents, .galeriaSBG #contents {width:100%;}.fotogrande #galeria, .galeriaSBG #galeriaSBG{background:url(images/fondo_imagen.png) repeat 0 0 transparent scroll;}.fotogrande #portada #galeria, .galeriaSBG #portada #galeriaSBG{background:none;}.galeriaSBG .wrapperPaginas {padding:0 2px 2px 0;}#controles {display:none;height:25px;left:-53%;margin-left:427px;position:absolute;right:0;text-align:right;top:170px;width:20px;z-index:100;}#bottom {width:100%;margin-top:50px;border-top:1px solid rgba(125,125,125,0.2);position:relative;}.portadaFull #bottom {display:none !important;}.fotogrande #bottom, .galeriaSBG #bottom { border:none;}#caption {position:absolute;left:3px; top:0;color:#BABABA;display:none;font-style:italic;width:50%;}#textFoot {height:30px;line-height:30px;width:50%;text-align:left;font-size:0.88em;}#linksFoot {position:absolute;right:0; top:4px;width:50%;text-align:right;z-index:5;}.portada #linksFoot{ top:22px; opacity:0.7; display:block !important; }.portada #textFoot{ text-align:right; width:100%; opacity:0.7; display:block !important; }.fotogrande #linksFoot, .galeriaSBG #linksFoot, .fotogrande #textFoot, .galeriaSBG #textFoot{ display:none; }.galbk {background-color:rgba(125,125,125,0.1);}@media screen and (max-width:64em) {.galbk {background-color:none;}}#preloader {position:absolute;left:400px;top:120px;}input, textarea {color:#BABABA;}a { color:#BABABA;text-decoration:none; }#contents .jspTrack {background-color:#141414;}#contents .jspDrag {background-color:#_bordercolorbottom_;}.bodyBKblog {padding-top:5px;}.bodyBKblog #contents {margin-top:60px;}@media screen and (max-width:48em) { .bk-touch body { font-size:16px } }.galeria_422 .thumbSBG { border:none; background:#EEEEEE; background:rgba(238,238,238, 0.14); border-radius:3px;}</style>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript">var uriDomain = 'https://ac.bluekea.com/demo-antares/';var uriUserSSL = 'https://ac.bluekea.com/demo-antares/';var uriUserSSLlang = 'https://ac.bluekea.com/demo-antares/';var uriBase = 'https://res.bluekea.com/account/';var uriBaseCDN = 'https://d3l48pmeh9oyts.cloudfront.net/';var langBK = '';var __codePortada = 'inicio';var uriCurrent = 'https://ac.bluekea.com/demo-antares/';var uriSChar = false;</script>
<script type="text/javascript" src="https://d3l48pmeh9oyts.cloudfront.net/min2/a_js__default__optim_top/197"></script>

<script type="text/javascript">
$(function() { menuSBG = $('#menu').sbgMenu({cssMenu:'default',colorAct:'#8C8C8C', colorNor:'#F0F0F0', colorHov:'#8C8C8C', primerCode:'inicio', blog: '0'}); })
</script>
<script type="text/javascript">

	function ajaxLoadPagina(_code) 
	{

		$('html').addClass('bk-loading-ajax');

		_code = typeof _code == 'undefined' ? '' : _code;

		$.ajax({
			url: 'https://ac.bluekea.com/demo-antares/index.php?ajax=1&c=' + _code,
			dataType: 'json',
			success: function(data, textStatus) {

				$(document).unbind("keydown.key-galbk");

				$('#controles').html('').hide();
				$('#caption, .galbk-elm').html('').hide();
				$('.compartirFoto').remove();

				setTitulo(data.tituloSEO);
				setCanonical(data.canonical);
				stopSpinner();

				$('body').removeClass().addClass(data.addClasses);

				$('.idiomasBK').html(data.idiomas);

				loadTituloPagina(data.targetTitulo, data.titulo);

				$('#' + data.targetContenidos).html(data.contenidos);
				$('#js_contents').html(data.contenidosJS);

				codigoJS();
				resumeMusic();
				sendAlert();

			},
			error: function(xhr, textStatus, errorThrown) {}
		});
	}
	

	var avisoCookBK = false;</script>
<meta property="og:url" content=""/>
<meta property="og:image" content=""/>
<meta property="og:type" content="website" /><meta property="og:image:width" content="1000" /> <meta property="og:image:height" content="567" /> <meta property="og:title" content="Retratos" />

</head>
<body class="inGal uniform inGalGroup">

<div id="innerBody">

<div id="container" class="wsetter">
	<div id="middleWrapper">
		<div id="middle">
			<div id="contents" class="fusible fixed_dim reset-mobile">
				<div id="sbg_contents">
					<div class="wrapper-galbk">
						<div class="galbk  galbk_inner galbk_strips galbk_386 " data-bk-numpaginas="1">
							<div class="lineBot spread">
								
							</div>
							<svg class="spread galbk-bot-base prev ">
								<use xlink:href="#i-angle-left-btm" />
							</svg>
							<svg class="spread galbk-bot-base next ">
								<use xlink:href="#i-angle-right-btm" />
							</svg><svg class="spread galbk-bot-base play ">
								<use xlink:href="#i-play" />
							</svg><svg class="spread galbk-bot-base pause ">
								<use xlink:href="#i-pause" />
							</svg>
							<svg class="spread galbk-bot-base close ">
								<use xlink:href="#i-times-btm" />
							</svg>
							<div class="shieldBK">
								
							</div>
						</div>
						<div class="galbk-elm galbk-titulo galbk-titulo-386 galbk-titulo-inner"></div>
					</div>
					<div id="galeria" data-bk-altomax="567" data-bk-ratio="0.74" data-bk-altomin="567" class="nojs tipo_strips pure-g galeria_386">
						<div class="pure-u-1-2 pure-u-sm-1-3 pure-u-md-1-4 pagina1  bkth bkth0"><a  data-bk-titulo="" href="img/1.jpg"  data-bk-ratio = "1.499" class="thumb thumb_4092" data-bk-index="0"><div class="thumb_bk"><div class="thumb_placeholder" style="padding-bottom: 68.57%">
							<img data-bk-id="4092"  src = "img/1.jpg">
							<div class="shieldTh">
								
							</div>
						</div>
					</div>
				</a>
			</div>
			<div class="pure-u-1-2 pure-u-sm-1-3 pure-u-md-1-4 pagina1  bkth bkth1"><a  data-bk-titulo="" href="img/2.jpg"  data-bk-ratio = "1.499" class="thumb thumb_4099" data-bk-index="1">
				<div class="thumb_bk">
					<div class="thumb_placeholder" style="padding-bottom: 68.57%">
						<img data-bk-id="4099"  src = "img/2.jpg">
						<div class="shieldTh">
							
						</div>
					</div>
				</div>
			</a>
		</div>


		<div class="pure-u-1-2 pure-u-sm-1-3 pure-u-md-1-4 pagina1  bkth bkth1"><a  data-bk-titulo="" href="img/3.jpg"  data-bk-ratio = "1.499" class="thumb thumb_4099" data-bk-index="1">
				<div class="thumb_bk">
					<div class="thumb_placeholder" style="padding-bottom: 68.57%">
						<img data-bk-id="4099"  src = "img/3.jpg">
						<div class="shieldTh">
							
						</div>
					</div>
				</div>
			</a>
		</div>


		<div class="pure-u-1-2 pure-u-sm-1-3 pure-u-md-1-4 pagina1  bkth bkth1"><a  data-bk-titulo="" href="img/2.jpg"  data-bk-ratio = "1.499" class="thumb thumb_4099" data-bk-index="1">
				<div class="thumb_bk">
					<div class="thumb_placeholder" style="padding-bottom: 68.57%">
						<img data-bk-id="4099"  src = "img/2.jpg">
						<div class="shieldTh">
							
						</div>
					</div>
				</div>
			</a>
		</div>


		</div>
	</div>
	<div id="js_contents"></div>			</div>
		</div>
		<div id="controles" class="controlesV"></div>
		<br class="cleaner" />
	</div>
	
	<div id="bottom" class="fusible reset-mobile">
		<div id="caption" class="moveInBottom"></div>
		<div id="linksFoot" class="hide-mobile"><div class="siguenosFA siguenosColor"><a title="Info" class="info-copyright" href="#" onclick="toggleInfoPie(); return false;"><svg class=""><use xlink:href="#i-copyright" /></svg></a></div></div>
		<div id="textFoot"></div>	
	</div>

</div>
<div id="copyright">&copy; Bluekea</div>
<script type="text/javascript">var codeGA = ''</script>
</div>


<script>
	function sbgIsHDdevice() {
	    var mediaQuery = "(-webkit-min-device-pixel-ratio: 1.5),\
	            (min--moz-device-pixel-ratio: 1.5),\
	            (-o-min-device-pixel-ratio: 3/2),\
	            (min-resolution: 1.5dppx)";
	    if (window.devicePixelRatio > 1)
	        return true;
	    if (window.matchMedia && window.matchMedia(mediaQuery).matches)
	        return true;
	    return false;
	}
	if (sbgIsHDdevice()) {
		document.cookie="bluekea_hddevice=ok";
	}
</script>

<div class="auxFusible"></div>
<div class="info-bk-foot"><span id="textoPie">Auge Punto central</span><span id="linkPB"></span><a href="#" class="closeinfobk" onclick="toggleInfoPie(); return false;"><svg class=""><use xlink:href="#i-long-arrow-down" /></svg></a> </div>
<script>
	$(function() {
	fadeInContent();;$('.galeria_386').galBluekea({ idPagina: 386, tipoGal: 'strips', tiempoFoto: 5000, loopPase: 0, transicion: 'fade',
												tiempoTransicion: 1000, autoStart: 0, designAsFrame: 2, presentacion: 'inner', 
												tipoMarco: 'solido', dimMarco: '50', colorMarco: '333333', sombra: 'off', colorFondo:'E9E9E9', alphaFondo: '0.8',
												cartOn: 'off', lblComprar: 'Comprar', lblCompartir: 'Compartir', lblVolver: 'Galeria', posBotonVenta: 'titulo', 
												'carousel': 0, 'compartirImg': 0,
												'fotoGrande': 0, 'noredim': false, showCopyRight: '1'
												});;esperarImgLoaded(386);activarFadeHoverImg(386);;;$('.botonesMusic').show();;makeIframeResponsive('#sbg_contents', '');;activarMenuComun();menuSBG.loadTitulo('Retratos');	})
	sendAlert();
</script>
<svg width="0" height="0" class="oculto-pos">
<symbol id="i-angle-left-btm" viewBox="0 0 576 1008"><g transform="matrix(1 0 0 -1 0 864)">
   <path d="M18 360l504 504l54 -54l-450 -450l450 -450l-54 -54z"/>
</g></symbol><symbol id="i-angle-right-btm" viewBox="0 0 576 1008"><g transform="matrix(1 0 0 -1 0 864)">
   <path d="M54 864l504 -504l-504 -504l-54 54l450 450l-450 450z"/>
</g></symbol><symbol id="i-arrow-right" viewBox="0 0 1008 1008"><g transform="matrix(1 0 0 -1 0 840)">
   <path d="M167 294v82h504l-230 233l59 59l333 -333l-333 -333l-59 59l230 233h-504z"/>
</g></symbol><symbol id="i-bars" viewBox="0 -50 864 1008"><path d="M864 684v-72c0 -20 -16 -36 -36 -36h-792c-20 0 -36 16 -36 36v72c0 20 16 36 36 36h792c20 0 36 -16 36 -36zM864 396v-72c0 -20 -16 -36 -36 -36h-792c-20 0 -36 16 -36 36v72c0 20 16 36 36 36h792c20 0 36 -16 36 -36zM864 108v-72c0 -20 -16 -36 -36 -36h-792 c-20 0 -36 16 -36 36v72c0 20 16 36 36 36h792c20 0 36 -16 36 -36z"/></symbol><symbol id="i-bars-btm" viewBox="0 0 1008 1008"><g transform="matrix(1 0 0 -1 0 864)">
   <path d="M1008 432v-72h-1008v72h1008zM1008 72v-72h-1008v72h1008zM1008 792v-72h-1008v72h1008z"/>
</g></symbol><symbol id="i-circle-o" viewBox="0 0 864 1008"><g transform="matrix(1 0 0 -1 0 864)">
   <path d="M864 360c0 -238 -193 -432 -432 -432s-432 194 -432 432s193 432 432 432v0c239 0 432 -194 432 -432zM432 666c-169 0 -306 -137 -306 -306s137 -306 306 -306s306 137 306 306s-137 306 -306 306z"/>
</g></symbol><symbol id="i-copyright" viewBox="0 0 864 1008"><g transform="matrix(1 0 0 -1 0 864)">
   <path d="M864 360c0 -238 -193 -432 -432 -432s-432 194 -432 432s193 432 432 432v0c239 0 432 -194 432 -432zM432 720c-199 0 -360 -161 -360 -360s161 -360 360 -360s360 161 360 360s-161 360 -360 360zM647 260v-61c0 -80 -127 -109 -206 -109c-154 0 -270 118 -270 273 c0 152 115 267 267 267c56 0 202 -20 202 -109v-61c0 -6 -4 -9 -9 -9h-67c-5 0 -9 4 -9 9v39c0 35 -68 52 -114 52c-105 0 -178 -76 -178 -185c0 -113 76 -196 182 -196c41 0 118 15 118 51v39c0 5 3 9 8 9h67c4 0 9 -4 9 -9z"/>
</g></symbol><symbol id="i-envelope" viewBox="0 0 1008 1008"><g transform="matrix(1 0 0 -1 0 864)">
   <path d="M1008 630c0 -63 -47 -120 -96 -154c-88 -61 -176 -122 -263 -183c-37 -25 -99 -77 -144 -77h-1h-1c-45 0 -107 52 -144 77c-87 61 -175 123 -262 183c-40 27 -97 90 -97 142c0 55 30 102 90 102h828c49 0 90 -40 90 -90zM1008 465v-447c0 -50 -40 -90 -90 -90h-828 c-50 0 -90 40 -90 90v447c17 -19 36 -35 57 -49c93 -64 188 -127 279 -194c48 -35 106 -78 167 -78h1h1c61 0 119 43 167 78c91 66 186 130 280 194c20 14 39 30 56 49z"/>
</g></symbol><symbol id="i-long-arrow-down" viewBox="0 0 576 1008"><g transform="matrix(1 0 0 -1 0 864)">
   <path d="M288 -144l-252 252l54 54l162 -162v864h72v-864l162 162l54 -54z"/>
</g></symbol><symbol id="i-long-arrow-up" viewBox="0 0 576 1008"><g transform="matrix(1 0 0 -1 0 864)">
   <path d="M36 612l252 252l252 -252l-54 -54l-162 162v-864h-72v864l-162 -162z"/>
</g></symbol><symbol id="i-menu" viewBox="0 0 1008 1008"><g transform="matrix(1 0 0 -1 0 819)">
   <path d="M1008 567h-1008v126h1008v-126zM1008 193h-1008v126h1008v-126zM1008 -185h-1008v126h1008v-126z"/>
</g></symbol><symbol id="i-pause" viewBox="0 0 576 1008"><g transform="matrix(1 0 0 -1 0 864)">
   <path d="M576 864v-1008h-144v1008h144zM144 864v-1008h-144v1008h144z"/>
</g></symbol><symbol id="i-play" viewBox="0 0 792 1008"><g transform="matrix(1 0 0 -1 0 864)">
   <path d="M779 343l-747 -416c-18 -9 -32 -1 -32 19v828c0 20 14 28 32 19l747 -416c17 -9 17 -25 0 -34z"/>
</g></symbol><symbol id="i-quote-entypo" viewBox="0 0 1008 1008"><g transform="matrix(1 0 0 -1 0 864)">
   <path d="M692 690l1 -3c85 0 143 -38 174 -115c29 -71 30 -155 2 -253s-76 -175 -144 -231c-55 -45 -115 -67 -180 -67v71c66 0 122 29 167 87c36 45 53 89 51 132c-2 49 -26 73 -71 73c-41 0 -76 15 -105 45s-43 66 -43 108s14 78 43 108s64 45 105 45zM268 693l1 -3 c85 0 143 -38 174 -115c29 -71 30 -155 2 -253s-76 -175 -144 -231c-55 -45 -115 -67 -180 -67v71c66 0 122 29 167 87c36 45 53 89 51 132c-2 49 -26 73 -71 73c-41 0 -76 15 -105 45s-43 66 -43 108s14 78 43 108s64 45 105 45z"/>
</g></symbol><symbol id="i-times-btm" viewBox="0 0 1008 1008"><g transform="matrix(1 0 0 -1 0 864)">
   <path d="M1008 810l-450 -450l450 -450l-54 -54l-450 450l-450 -450l-54 54l450 450l-450 450l54 54l450 -450l450 450z"/>
</g></symbol></svg></body>
</html>