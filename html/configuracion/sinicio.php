<!DOCTYPE html>
<html lang="en">
  <head>
    <!--Los estilos-->
    <?php include 'html/overall/header.php'; ?>
  </head>
  <body>
    <!--menu-->
    <?php
    include 'html/overall/topnav.php';
    ?>
    <!--menu-->
    <div class="container-fluid">
      <br><br><br>
    	<h3>Bienvenido <?php echo $sesion[$_SESSION['APP_ADMIN']]['nombre']; echo "&nbsp;"; echo $sesion[$_SESSION['APP_ADMIN']]['apellidos']; ?></h3>
    </div>
    <!--scripts-->
    <?php include 'html/overall/footer.php'; ?>
    <script src="js/public/sesion.js" ></script>
    <!--scripts-->
  </body>
</html>
