<!DOCTYPE html>
<html lang="en">
  <head>
    <?php
    include 'html/overall/header.php';
    ?>
  </head>
  <body>
    <?php
    include 'html/overall/topnav.php';
    ?>
    <div class="container">
    <div class="row">
                <div class="col-md-8">
                <form class="form-horizontal" method="post">
                     <div class="form-group">
                            <span class="col-md-1 col-md-offset-2"></span>
                            <div class="col-md-8">
                               <h3>AGENDA TU EVENTO</h3>
                            </div>
                        </div>
                        <div class="form-group">
                            <span class="col-md-1 col-md-offset-2"><i class="glyphicon glyphicon-user"></i></span>
                            <div class="col-md-8">
                                <input id="fname" name="name" type="text" placeholder="Nombre" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <span class="col-md-1 col-md-offset-2"><i class="glyphicon glyphicon-home"></i></span>
                            <div class="col-md-8">
                                <input id="lname" name="name" type="text" placeholder="Empresa" class="form-control">
                            </div>
                        </div>

                        <div class="form-group">
                            <span class="col-md-1 col-md-offset-2"><i class="glyphicon glyphicon-phone"></i></span>
                            <div class="col-md-8">
                                <input id="phone" name="phone" type="text" placeholder="Telefono" class="form-control">
                            </div>
                        </div>

                        <div class="form-group">
                            <span class="col-md-1 col-md-offset-2"><i class="glyphicon glyphicon-paperclip"></i></span>
                            <div class="col-md-8">
                                <input id="email" name="email" type="text" placeholder="Email" class="form-control">
                            </div>
                        </div>

                        <div class="form-group">
                            <span class="col-md-1 col-md-offset-2"><i class="glyphicon glyphicon-text-background"></i></span>
                            <div class="col-md-8">
                                <textarea class="form-control" id="message" name="message" placeholder="Escribe tu mensaje.." rows="7"></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-1 col-md-offset-2">
                                <button type="submit" class="btn btn-info">Enviar mensaje</button>
                            </div>
                        </div>
                </form>
            </div>
        <div class="col-md-4">
            <div class="form-group">
                <div class="col-md-1 col-md-offset-2">
                    <H3>CONTACTO</H3>
                </div>
            </div>
        </div>
    </div>
</div>
  <script src="js/public/registrarme.js"> </script>
    <?php include 'html/overall/footer.php'; ?>
  </body>
</html>

