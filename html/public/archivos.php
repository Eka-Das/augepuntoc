<!DOCTYPE html>
<html lang="en">
  <head>
    <!--Los estilos-->
    <?php include 'html/overall/header.php'; ?>
  </head>
  <body>
    <!--menu-->
    <?php
    include 'html/overall/topnav.php';
    ?>
    <!--menu-->
    <div class="container-fluid">
      <h3>Subida de archivos</h3>
      <div class="row">
        <div class="col-md-12">
          <div>
          <!-- Nav tabs -->
          <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Inicio</a></li>
            <li role="presentation"><a href="#eventos" aria-controls="profile" role="tab" data-toggle="tab">Eventos</a></li>
            <li role="presentation"><a href="#restaurantes" aria-controls="profile" role="tab" data-toggle="tab">Restaurantes</a></li>
            <li role="presentation"><a href="#secciones" aria-controls="profile" role="tab" data-toggle="tab">Secciones</a></li>
            <li role="presentation"><a href="#videos" aria-controls="profile" role="tab" data-toggle="tab">Videos</a></li>
            <li role="presentation"><a href="#portadas" aria-controls="profile" role="tab" data-toggle="tab">Portadas</a></li>
            <li role="presentation"><a href="#contacto" aria-controls="profile" role="tab" data-toggle="tab">Contacto</a></li>
            <li role="presentation"><a href="#menu" aria-controls="profile" role="tab" data-toggle="tab">Agregar menú</a></li>
          </ul>
          <!-- Tab panes -->
          <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="home">Aqui van a estar todos los elementos a subir en el index</div>
            <div role="tabpanel" class="tab-pane" id="eventos">Aqui van a estar los elementos a subir desde eventos</div>
            <div role="tabpanel" class="tab-pane" id="restaurantes">......</div>
            <div role="tabpanel" class="tab-pane" id="secciones">......</div>
            <div role="tabpanel" class="tab-pane" id="videos">......</div>
            <div role="tabpanel" class="tab-pane active" id="portadas">
            <div class="row">
              <div class="col-md-4">
                <h3>Subir informacion de portadas.</h3>
                <form class="form-horizontal">
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Seleccionar imagen</label>
                    <div class="col-sm-10">
                      <input type="file" class="form-control" id="img">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputPassword3" class="col-sm-2 control-label">Titulo de portada</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="inputPassword3" placeholder="Titulo">
                    </div>
                  </div>
                  
                  <div class="form-group">
                    <label for="inputPassword3" class="col-sm-2 control-label">Subtitulo</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="inputPassword3" placeholder="Escribe un titulo">
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="inputPassword3" class="col-sm-2 control-label">Descripcion</label>
                    <div class="col-sm-10">
                      <textarea class="form-control" placeholder="Escribe una descripcion de la portada.."></textarea>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                      <button type="submit" class="btn btn-success">Subir archivo</button>
                    </div>
                  </div>
                </form>
              </div>
              <div class="col-md-4">
                <h3>Portadas registradas</h3>
                <div class="bs-example" data-example-id="condensed-table">
                  <table class="table table-condensed">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>First Name</th>
                      <th>Last Name</th>
                      <th>Username</th>
                      <th>Accion</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <th scope="row">1</th>
                      <td>Mark</td>
                      <td>Otto</td>
                      <td>@mdo</td>
                      <td>
                        <a class="btn btn-danger btn-xs" href="">Eliminar</a>
                      </td>
                    </tr>
                    <tr>
                      <th scope="row">2</th>
                      <td>Jacob</td>
                      <td>Thornton</td>
                      <td>@fat</td>
                      <td>
                        <a class="btn btn-danger btn-xs" href="">Eliminar</a>
                      </td>
                    </tr>
                    <tr>
                      <th scope="row">3</th>
                      <td colspan="2">Larry the Bird</td>
                      <td>@twitter</td>
                      <td>
                        <a class="btn btn-danger btn-xs" href="">Eliminar</a>
                      </td>
                    </tr>
                  </tbody>
                </table>
                </div>
              </div>
            </div>
            </div>
            <div role="tabpanel" class="tab-pane" id="contacto">......</div>
            <div role="tabpanel" class="tab-pane" id="menu">......</div>
          </div>
        </div>
        </div>
      </div>
      <hr>
    </div>
    <!--scripts-->
    <?php include 'html/overall/footer.php'; ?>
    <script src="js/public/sesion.js" ></script>
    <!--scripts-->
  </body>
</html>
