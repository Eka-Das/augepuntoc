<!DOCTYPE html>
<html lang="en">
  <head>
    <!--Los estilos-->
    <?php include 'html/overall/header.php'; ?>
    <style type="text/css">
      .thumbnail {
    border: 0 none;
    box-shadow: none;
}
    </style>
  </head>
  <body>
    <!--menu-->
    <?php
    include 'html/overall/topnav.php';
    ?>
    <!--menu-->
    <div class="container-fluid">
      <h3 class="text-center">Portadas</h3>
       <div class="row portfolio">
        <div class="col-sm-6 col-md-3">
          <div class="thumbnail">
            <img class="img-responsive" src="http://augepuntocentral.com/img/columna/515/38dfaacd911796931af6f6beb72b8eff.jpg" alt="The awesome description" data-toggle="modal" data-target="#myModal">
            <div class="caption">
              <div class="clearfix">KETAR GÓMEZ Y RODRIGO ESPINOSA</div>
              <p>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                consequat. 
              </p>
              <a href="index.php?view=descripcion" class="btn btn-default">Ver mas</a>
            </div>
          </div>
        </div>

      <div class="col-sm-6 col-md-3">
          <div class="thumbnail">
            <img class="img-responsive" src="http://augepuntocentral.com/img/columna/514/31c0b57f511539c95fa79005db64fed3.jpg" alt="The awesome description" data-toggle="modal" data-target="#myModal">
            <div class="caption">
              <div class="clearfix">JOSÉ CADENA Y ANUAR GARAY</div>
              <p>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                consequat. 
              </p>
              <a href="index.php?view=descripcion" class="btn btn-default">Ver mas</a>
            </div>
          </div>
        </div>
        
        <div class="col-sm-6 col-md-3">
          <div class="thumbnail">
            <img class="img-responsive" src="http://augepuntocentral.com/img/columna/511/bb5421e9bac713dab6fe5ff7c4e1efd2.jpg" alt="The awesome description" data-toggle="modal" data-target="#myModal">
            <div class="caption">
              <div class="clearfix">ALEJANDRO FERNÁNDEZ Y MARÍA LORCA</div>
              <p>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                consequat. 
              </p>
              <a href="index.php?view=descripcion" class="btn btn-default">Ver mas</a>
            </div>
          </div>
        </div>
        <div class="col-sm-6 col-md-3">
          <div class="thumbnail">
            <img class="img-responsive" src="http://augepuntocentral.com/img/columna/511/bb5421e9bac713dab6fe5ff7c4e1efd2.jpg" alt="The awesome description" data-toggle="modal" data-target="#myModal">
            <div class="caption">
              <div class="clearfix">ALEJANDRO FERNÁNDEZ Y MARÍA LORCA</div>
              <p>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                consequat. 
              </p>
              <a href="index.php?view=descripcion" class="btn btn-default">Ver mas</a>
            </div>
          </div>
        </div>
      <!-- Modal -->
      <!--div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
              <h4 class="modal-title" id="myModalLabel">Aqui ira el titulo de la imagen</h4>
            </div>
            <div class="modal-body">
              <img class="img-responsive" src="http://lorempixel.com/600/400/cats/?42" alt="The awesome description">
            </div>
          </div>
        </div>
      </div-->
    </div>
  </div>
    <!--scripts-->
    <?php include 'html/overall/footer.php'; ?>
    <script src="js/public/sesion.js" ></script>
    <!--scripts-->
  </body>
</html>
