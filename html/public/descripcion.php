<!DOCTYPE html>
<html lang="en">
  <head>
    <!--Los estilos-->
    <?php include 'html/overall/header.php'; ?>
  </head>
  <body>
    <!--menu-->
    <?php
    include 'html/overall/topnav.php';
    ?>
    <!--menu-->
    <div class="container">
      <h3 class="text-center">Descripcion de la portada</h3>
      <div class="row">
        <div class="col-md-8">
          <!---->
          <div class="row portfolio">
        <div class="col-sm-6">
          <div class="thumbnail">
            <img class="img-responsive" src="http://lorempixel.com/600/400/cats/?1" alt="The awesome description" data-toggle="modal" data-target="#myModal">
            <div class="caption">
              <div class="clearfix">Aqui va la descripcion de las imagenes</div>
            </div>
          </div>
        </div>

        <div class="col-sm-6">
          <div class="thumbnail">
            <img class="img-responsive" src="http://lorempixel.com/600/400/cats/?2" alt="The awesome description" data-toggle="modal" data-target="#myModal">
            <div class="caption">
              <div class="clearfix">Aqui va la descripcion de las imagenes</div>
            </div>
          </div>
        </div>

        <div class="col-sm-6">
          <div class="thumbnail">
            <img class="img-responsive" src="http://lorempixel.com/600/400/cats/?3" alt="The awesome description" data-toggle="modal" data-target="#myModal">
            <div class="caption">
              <div class="clearfix">Aqui va la descripcion de las imagenes</div>
            </div>
          </div>
        </div>

        <div class="col-sm-6">
          <div class="thumbnail">
            <img class="img-responsive" src="http://lorempixel.com/600/400/cats/?4" alt="The awesome description" data-toggle="modal" data-target="#myModal">
            <div class="caption">
              <div class="clearfix">Aqui va la descripcion de las imagenes</div>
            </div>
          </div>
        </div>

        <div class="col-sm-6">
          <div class="thumbnail">
            <img class="img-responsive" src="http://lorempixel.com/600/400/cats/?5" alt="The awesome description" data-toggle="modal" data-target="#myModal">
            <div class="caption">
              <div class="clearfix">Aqui va la descripcion de las imagenes</div>
            </div>
          </div>
        </div>

        <div class="col-sm-6 ">
          <div class="thumbnail">
            <img class="img-responsive" src="http://lorempixel.com/600/400/cats/?6" alt="The awesome description" data-toggle="modal" data-target="#myModal">
            <div class="caption">
              <div class="clearfix">Aqui va la descripcion de las imagenes</div>
            </div>
          </div>
        </div>
      <!-- Modal -->
      <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
              <h4 class="modal-title" id="myModalLabel">Aqui ira el titulo de la imagen</h4>
            </div>
            <div class="modal-body">
              <img class="img-responsive" src="http://lorempixel.com/600/400/cats/?42" alt="The awesome description">
            </div>
          </div>
        </div>
      </div>
    </div>
          <!---->
       </div>
        <div class="col-md-4">
          <div class="thumbnail">
          <img src="http://lorempixel.com/600/400/cats/?1" alt="..." width="100%">
          <div class="caption text-center">
            <h4>Titulo de la portada</h4>
          </div>
          <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
          tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
          quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
          consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
          cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
          proident, sunt in culpa qui officia deserunt mollit anim id est laborum. </p>
        </div>
        </div>  
      </div>
    </div>
    <!--scripts-->
    <?php include 'html/overall/footer.php'; ?>
    <script src="js/public/sesion.js" ></script>
    <!--scripts-->
  </body>
</html>
