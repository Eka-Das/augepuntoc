<!DOCTYPE html>
<html lang="en">
  <head>
    <!--Los estilos-->
    <?php include 'html/overall/header.php'; ?>
    <style type="text/css">

      @import url(https://fonts.googleapis.com/css?family=Bellefair|Raleway:300,700);

      .galbk-bot-base.spread.prev, .galbk-bot-base.spread.next{
        background-color:#FFFFFF;
        background-color:rgba(255,255,255,0.4);
      }



      .forml{
        display: block;
        margin-block-start: 1em;
        margin-block-end: 1em;
        margin-inline-start: 0px;
        margin-inline-end: 0px;
        color:#474747;
        font-family:Bellefair, Sans-Serif, Verdana;
        font-size:18px;
        margin-bottom:50px;
        background-color:#FFFFFF;
      }

      html {
        -webkit-text-size-adjust: 100%;
      }
      .ver {
      position: relative /* o absolute*/;
      top: 400px;
      right:-50px;
      width:120px;
      z-index:99;
      }
    </style>
  </head>
  <body>
    <!--menu-->
    <?php
    include 'html/overall/topnav.php';
    ?>
    <!--menu-->
    <div class="container">
      <br><br>
      <h3 class="text-center">Perspectiva</h3>
      <div class="row">
        <div class="col-md-12">
          <img src="img/secciones.jpg" alt="" width="100%;">
        </div>
      </div>
      <br>
      <div class="row">
        <div class="col-md-9">
          <div id="myCarousel" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
              <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
              <li data-target="#myCarousel" data-slide-to="1"></li>
            </ol>
            <div class="carousel-inner">
              <div class="item active">
                <img src="http://augepuntocentral.com/img/columna/515/38dfaacd911796931af6f6beb72b8eff.jpg"  width="100%;">
              </div>
              <div class="item">
                <img src="http://augepuntocentral.com/img/columna/514/31c0b57f511539c95fa79005db64fed3.jpg"  width="100%;">
              </div>
            </div>
            <a class="left carousel-control" href="#myCarousel" data-slide="prev">
              <span class="glyphicon glyphicon-chevron-left"></span>
              <span class="sr-only">Anterior</span>
            </a>
            <a class="right carousel-control" href="#myCarousel" data-slide="next">
              <span class="glyphicon glyphicon-chevron-right"></span>
              <span class="sr-only">Siguiente</span>
            </a>
          </div>
          <br>
        </div>

        <div class="col-md-3">
          <div class="">
            <img src="img/3.jpg" class="img-responsive" alt="">
            <p class="forml">
              Esta es una pequeña descripcion de la fecha: 2019-01-12
            </p>

          </div>
          <div class="">
            <img src="img/a4.jpg" class="img-responsive" alt="">
            <p class="forml">
              Esta es una pequeña descripcion de la fecha: 2019-01-12
            </p>

          </div>
        </div>
      </div>
      <div class="row">
         <div class="col-md-12">
           <h3>Ser feliz para producir más:</h3>
           <p class="forml">
             DENTRO DE ESTA SECCIÓN SE TRATAN TEMAS CONFORME A LA PROFESIÓN U OCUPACIÓN DEL COLABORADOR, ES DECIR, EL COLABORADOR TRATA UN TEMA QUE ESTÉ EN BOGA EN SU ÁREA EN LA QUE SE DESENVUELVE DÍA A DÍA. EL OBJETIVO INICIAL DE ESTA SECCIÓN ES DAR A CONOCER EL PUNTO
           </p>
           <p class="forml">
             En esta edición de Perspectiva, traemos un artículo de la revista Forbes México, en el cual se hace una revisión sobre la importancia de la felicidad para nuestra productividad en la oficina.

A través de los años, el panorama laboral se ha transformado al grado de que el principal indicador de satisfacción en el trabajo ya no es el sueldo, sino la importancia de desarrollarse dentro de un sano ambiente laboral, mismo que engloba factores como calidad de vida, crecimiento personal, retos medibles y reconocimiento, que influirán en la decisión del colaborador para permanecer o no dentro de una organización.

¿Cómo se puede mejorar de manera efectiva el ambiente laboral para lograr la fidelización y permanencia de los empleados? Generando bienestar y compromiso, pues sabemos que las personas que disfrutan de su trabajo gozan más de la vida, tienen una mejor salud y objetivos de vida claros, por lo que impactan positivamente en la empresa al influir directamente en la creación de equipos de trabajo productivos, comprometidos y sobre todo felices.
           </p>
         </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <img src="img/secciones.jpg" alt="" width="100%;">
        </div>
      </div>
    </div>
    <hr>
    <!--scripts-->
    <?php include 'html/overall/footer.php'; ?>
    <script src="js/public/sesion.js" ></script>
    <!--scripts-->
  </body>
</html>
