<!DOCTYPE html>
<html lang="en">
  <head>
    <!--Los estilos-->
    <?php include 'html/overall/header.php'; ?>
  </head>
  <body>
    <!--menu-->
    <?php
    include 'html/overall/topnav.php';
    ?>
    <!--menu-->
    <div class="container-fluid">
      <h3 class="text-center">Eventos Sociales</h3>
      <div class="row">
        <div class="col-md-4">
         <a href="index.php?view=">
            <div class="thumbnail">
              <img src="img/1.jpg" alt="...">
              <div class="caption text-center">
                <h4>BAY SHOWER DE MARCELA DÍA INFANTE</h4>
              </div>
            </div>
         </a>
        </div>
        <div class="col-md-4">
          <a href="index.php?view=">
            <div class="thumbnail">
              <img src="img/2.jpg" alt="...">
              <div class="caption text-center">
                <h4>BAY SHOWER DE MARCELA DÍA INFANTE</h4>
              </div>
            </div>
          </a>
        </div>
        <div class="col-md-4">
          <a href="index.php?view=">
            <div class="thumbnail">
              <img src="img/3.jpg" alt="...">
              <div class="caption text-center">
                <h4>BAY SHOWER DE MARCELA DÍA INFANTE</h4>
              </div>
            </div>
          </a>
        </div> 
      </div>
    </div>
    <!--scripts-->
    <?php include 'html/overall/footer.php'; ?>
    <script src="js/public/sesion.js" ></script>
    <!--scripts-->
  </body>
</html>
