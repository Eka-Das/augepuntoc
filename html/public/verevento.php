<!DOCTYPE html>
<html lang="en">
  <head>
    <!--Los estilos-->
    <?php include 'html/overall/header.php'; ?>
    <style media="screen">
      .shad{
        -webkit-box-shadow: 4px 18px 27px -8px rgba(0,0,0,0.75);
-moz-box-shadow: 4px 18px 27px -8px rgba(0,0,0,0.75);
box-shadow: 4px 18px 27px -8px rgba(0,0,0,0.75);
      }
    </style>
  </head>
  <body>
    <!--menu-->
    <?php
    include 'html/overall/topnav.php';
    ?>
    <!--menu-->
    <div class="container">
      <br> <br><br>
      <div class="row">
          <div class="col-md-12 shad">
            <img src="img/lateral1.jpg" alt="" width="100%;">
          </div>
      </div>
      <hr>
    <div class="row">
      <div class="col-md-12">
        <a href="index.php?view=principal" class="btn btn-info" style="font-size:16px;"> <span class="glyphicon glyphicon-home"></span> Regresar</a>
        <h3 class="text-center ">Eventos sociales</h3>
        <h3>Aqui va a ir el texto de este evento</h3>
      </div>
    </div>
      <br>
      <div class="row">
        <div class="col-md-12">
          <div id="mdb-lightbox-ui"></div>

          <div class="mdb-lightbox no-margin">

            <div class="col-md-4">
              <a href="#" data-toggle="modal" data-target="#myModal">
                <img alt="picture" src="https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(117).jpg" class="img-fluid" width="100%;">
              </a>
            </div>

            <div class="col-md-4">
                <img alt="picture" src="https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(98).jpg"
                  class="img-fluid" width="100%;" />
            </div>

            <div class="col-md-4">
                <img alt="picture" src="https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(131).jpg"
                class="img-fluid" width="100%;" />
            </div>

            <div class="col-md-4">
                <img alt="picture" src="https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(123).jpg"
              class="img-fluid" width="100%;" />
            </div>

            <div class="col-md-4">
                <img alt="picture" src="https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(118).jpg"
                  class="img-fluid" width="100%;" />
            </div>

            <div class="col-md-4">
                <img alt="picture" src="https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(128).jpg"
                  class="img-fluid" width="100%;" />
            </div>

            <div class="col-md-4">
                <img alt="picture" src="https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(132).jpg"
                  class="img-fluid" width="100%;" />
            </div>

            <div class="col-md-4">
                <img alt="picture" src="https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(115).jpg"
                  class="img-fluid" width="100%;" />
            </div>

            <div class="col-md-4">
                <img alt="picture" src="https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(133).jpg"
                  class="img-fluid" width="100%;" />
            </div>

          </div>
        </div>
      </div>
      <hr>
      <div class="row">
          <div class="col-md-12 shad">
            <img src="img/lateral2.jpg" alt="" width="100%;">
          </div>
      </div>
      <hr>
      <div aria-hidden="true" aria-labelledby="myModalLabel" class="modal fade" id="myModal" role="dialog" tabindex="-1" style="display: none;">
        <div class="modal-dialog modal-lg modal-dialog-centered">
          <div class="modal-content" style="background-color:black;">
              <div class="modal-header"><button class="close" data-dismiss="modal" title="Close" type="" style="color:white;"><span class="glyphicon glyphicon-remove"></span></button></div>
              <div class="">
                <div class="carousel slide" data-interval="false" id="myGallery">
                  <div class="carousel-inner">
                    <div class="item active" >

                      <img alt="item1" src="img/1.jpg" width="100%">

                      <div class="text-center" style="color:white; background-color:white;">
                        <h4> Este es el nombre de la imagen </h4>
                      </div>
                    </div>
                    <div class="item">
                      <img alt="item2" src="img/2.jpg" width="100%">
                    </div>
                  </div>
                <a class="left carousel-control" data-slide="prev" href="#myGallery" role="button">
                  <span class="glyphicon glyphicon-chevron-left">
                  </span>
                </a>
                <a class="right carousel-control" data-slide="next" href="#myGallery" role="button">
                  <span class="glyphicon glyphicon-chevron-right">
                  </span>
                </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>


    </div>
    <!--scripts-->
    <?php include 'html/overall/footer.php'; ?>
    <!--scripts-->

  </body>
  <script type="text/javascript">
  // MDB Lightbox Init
  $(function () {
  $("#mdb-lightbox-ui").load("mdb-addons/mdb-lightbox-ui.html");
  });
  </script>
</html>
