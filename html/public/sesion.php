<!DOCTYPE html>
<html lang="en">
  <head>
    <!--Los estilos-->
    <?php include 'html/overall/header.php'; ?>
  </head>
  <body>
    <?php
    include 'html/overall/topnav.php';
    ?>
    <!--menu-->
    <div class="container">
      <br><br><br>
      <div class="row">
        <div class="col-md-4 col-md-offset-3">
          <div class="text-center" id="fLogin">
          </div>
        </div>
      </div>
      <div class="row">
      <div class="col-md-4 col-md-offset-3">
        <div class="panel panel-login">
          <div class="panel-heading">
            <div class="row">
              <div class="col-md-12 text-center">
                <img src="img/logo/ico.png" alt="" style="" width="10%">
              </div>
            </div>
            <hr>
          </div>
          <div class="panel-body">
            <div class="row">
              <div class="col-lg-12">
                <form id="login-form" action="" method="post" role="form">
                  <div class="form-group">
                    <input type="text" name="username" id="username" tabindex="1" class="form-control" placeholder="Usuario" value="">
                  </div>
                  <div class="form-group">
                    <input type="password" name="password" id="password" tabindex="2" class="form-control" placeholder="Contraseña">
                  </div>
                  <div class="form-group">
                    <div class="row">
                      <div class="col-md-6">

                        <div class="form-group">
                          <input type="checkbox" tabindex="3" class="" name="remember" id="remember">
                          <label for="remember"> Recordarme</label>
                        </div>
                      </div>
                      <div class="col-lg-6">
                          <input type="button" name="login-submit" id="login-submit" class="form-control btn btn-success" value="Iniciar sesión" onclick="sesion();">
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="row">
                      <div class="col-lg-12 text-center">
                        <div class="">
                          <a href="index.php?view=registrarme" tabindex="5" class="forgot-password">Registrarme</a>
                        </div>
                        <div class="">
                          <a href="index.php?view=recordar" tabindex="5" class="forgot-password">¿Recordar Contraseña?</a>
                        </div>
                      </div>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
    <!--scripts-->
    <?php include 'html/overall/footer.php'; ?>
    <script src="js/public/sesion.js" ></script>
    <!--scripts-->
  </body>
</html>
sufo
