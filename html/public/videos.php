<!DOCTYPE html>
<html lang="en">
  <head>
    <!--Los estilos-->
    <?php include 'html/overall/header.php'; ?>
  </head>
  <body>
    <!--menu-->
    <?php
    include 'html/overall/topnav.php';
    ?>
    <!--menu-->
    <div class="container">
      <br><br>
      <h3 class="text-center">Videos de portadas</h3>
      <div class="row">
        <div class="col-md-4">
          <b>Te Recomendamos - Golden Glove</b>
          <div class="embed-responsive embed-responsive-16by9">
            <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/v64KOxKVLVg" allowfullscreen></iframe>
          </div>

        </div>
      </div>
    </div>
    <hr>
    <!--scripts-->
    <?php include 'html/overall/footer.php'; ?>
    <script src="js/public/sesion.js" ></script>
    <!--scripts-->
  </body>
</html>
<?php

?>
