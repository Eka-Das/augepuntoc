<!DOCTYPE html>
<html lang="en">
<head>
  <?php include 'html/overall/header.php'; ?>
  <style type="text/css">

    @import url(https://fonts.googleapis.com/css?family=Bellefair|Raleway:300,700);

    .galbk-bot-base.spread.prev, .galbk-bot-base.spread.next{
      background-color:#FFFFFF;
      background-color:rgba(255,255,255,0.4);
    }

    body {
      color:#474747;
      font-family:Bellefair, Sans-Serif, Verdana;
      font-size:18px;
      margin-bottom:50px;
      background-color:#FFFFFF;
    }

    p{
      display: block;
      margin-block-start: 1em;
      margin-block-end: 1em;
      margin-inline-start: 0px;
      margin-inline-end: 0px;
    }

    html {
      -webkit-text-size-adjust: 100%;
    }
    .ver {
    position: relative /* o absolute*/;
    top: 400px;
    right:-50px;
    width:120px;
    z-index:99;
    }
  </style>
</head>
<body>
  <div class="logo" style="text-align: center;">
    <img src="img/logo/ico.png" width="7%" >
  </div>
  <br>
  <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
      <li data-target="#myCarousel" data-slide-to="2"></li>
    </ol>
    <div class="carousel-inner">
      <div class="item active">
        <img src="http://augepuntocentral.com/img/columna/515/38dfaacd911796931af6f6beb72b8eff.jpg"  width="100%;">
      </div>
      <div class="item">
        <img src="http://augepuntocentral.com/img/columna/514/31c0b57f511539c95fa79005db64fed3.jpg"  width="100%;">
      </div>
    </div>
    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left"></span>
      <span class="sr-only">Anterior</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right"></span>
      <span class="sr-only">Siguiente</span>
    </a>
  </div>
  <br> <br>
  <div class="container">
    <div class="row text-center">
      <div>
        <a href="index.php?view=principal" class="btn btn-info btn-lg">Continuar</a>
      </div>
    </div>
    <div class="row">
      <div class="col-md-3"></div>
      <div class="col-md-6">
       <p class="text-center">
        (Esto solo es un ejemplo.)
        Oprah Winfrey nació en Misisipi el 29 de enero de 1954. Se formó como periodista y presentadora de televisión; además de ser crítica de libros. Fue varias veces ganadora del Premio Emmy por su programa The Oprah Winfrey Show, el programa de entrevistas más visto en la historia de la televisión. ​ Además es una influyente crítica de libros, actriz nominada a un Premio Óscar y editora de su propia revista. Según la revista Forbes, fue la persona afroamericana más rica del siglo XX y la única de origen negro en el mundo, en poseer más de mil millones de dólares durante tres años consecutivos. También se dice que fue la mujer más poderosa del año 2005, según Forbes.
      </p>
    </div>
    <div class="col-md-3"><!Aqui no va nada></div>
  </div>
  <br><br><br>
  <br>
  <div class="row">
    <div class="col-md-4">
        <div data-toggle="modal" data-target="#myModal">
          <a href="#myGallery" data-slide-to="0">
            <img class="img-thumbnail" src="img/1.jpg" width="100%">
          </a>
        </div>
    </div>
    <div class="col-md-4">
      <div data-toggle="modal" data-target="#myModal">
        <a href="#myGallery" data-slide-to="1">
          <img class="img-thumbnail" src="img/2.jpg" width="100%">
        </a>
      </div>
    </div>
    <div class="col-md-4">
      <div data-toggle="modal" data-target="#myModal">
        <a href="#myGallery" data-slide-to="2">
          <img class="img-thumbnail" src="img/3.jpg" width="100%">
        </a>
      </div>
    </div>
  </div>
  <div aria-hidden="true" aria-labelledby="myModalLabel" class="modal fade" id="myModal" role="dialog" tabindex="-1" style="display: none;">
    <div class="modal-dialog modal-lg modal-dialog-centered">
      <div class="modal-content">
          <!-div class="modal-header"><button class="close" data-dismiss="modal" title="Close" type=""><span class="glyphicon glyphicon-remove"></span></button></div->
          <div class="">
            <div class="carousel slide" data-interval="false" id="myGallery">
              <div class="carousel-inner">
                <div class="item active">
                  <img alt="item1" src="img/1.jpg" width="100%">
                </div>
                <div class="item">
                  <img alt="item2" src="img/2.jpg" width="100%">
                </div>
              </div>
            <a class="left carousel-control" data-slide="prev" href="#myGallery" role="button">
              <span class="glyphicon glyphicon-chevron-left">
              </span>
            </a>
            <a class="right carousel-control" data-slide="next" href="#myGallery" role="button">
              <span class="glyphicon glyphicon-chevron-right">
              </span>
            </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
<script src="css/js/jquery-3.2.1.min.js"></script>
<script src="css/bootstrap/js/bootstrap.min.js"></script>
</body>
</html>
