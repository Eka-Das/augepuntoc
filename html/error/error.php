<!DOCTYPE html>
<html lang="en">
  <head>
    <!--Los estilos-->
    <?php include 'html/overall/header.php'; ?>
  </head>
  <body>
    <!--Modals-->
    <?php include 'html/modals/sesion.html'; ?>
    <!--Modals-->

    <!--menu-->
    <?php include 'html/overall/topnav.php'; ?>
    <!--menu-->
    <div class="container">
      <div class="row">
				<img src="img/error404.jpg" width="100%">
      </div>
			<hr>
			<div class="row">
				<div class="alert alert-warning alert-dismissible" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<strong>Alerta!</strong> No se encontro busqueda
					<a href="index.php?view=index">¿Regresar al inicio?</a>
				</div>
			</div>
    </div>
    <!--scripts-->
    <?php include 'html/overall/footer.php'; ?>
    <!--scripts-->
  </body>
</html>
