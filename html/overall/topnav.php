<nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="container-fluid">
          <div class="navbar-header">
            <a class="navbar-brand" href="index.php"><img src="img/logo/logoAugeWhite.png" alt="" style="float: left;"></a>

            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
          </div>
         <?php
         if (isset($_SESSION['APP_ADMIN'])) {
           $sesion = sesion();
           ?>
           <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
              <li class="active"><a href="index.php?view=sinicio">Inicio</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                  <?php echo $sesion[$_SESSION['APP_ADMIN']]['nombre'];?> <?php echo $sesion[$_SESSION['APP_ADMIN']]['apellidos'];?>
                  <span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li><a href="index.php?view=configuracion">Configuracion</a></li>
                  <li><a href="index.php?view=perfil">Perfil</a></li>
                  <li role="separator" class="divider"></li>
                  <li><a href="control/bin/ajax/destroy.php">Salir</a></li>
                </ul>
              </li>
            </ul>
          </div>
           <?php
         }else{
          ?>
           <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
              <li class="active"><a href="index.php?view=index">Inicio</a></li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Eventos<span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li><a href="index.php?view=eventos">Eventos sociales</a></li>
                </ul>
              </li>
              <li><a href="index.php?view=restaurante">Restaurantes</a></li>
               <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Secciones <span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li><a href="index.php?view=secciones">Perspectiva</a></li>
                </ul>
              </li>
              <li><a href="index.php?view=videos">Videos</a></li>
              <li><a href="index.php?view=portada">Portadas</a></li>
              <li><a href="index.php?view=contactanos">Contacto</a></li>
            </ul>
            <form class="navbar-form navbar-right">
                <div class="form-group">
                  <input type="text" class="form-control" placeholder="Buscar evento...">
                </div>
                <a  class="btn btn-default" href="index.php?view=buscar">Buscar</a>
              </form>
            <!--form class="navbar-form navbar-right">
        <div class="form-group">
        <a href="index.php?view=sesion" class="btn btn-info">Iniciar sesion</a>
      </form-->
            <!--ul class="nav navbar-nav navbar-right">
              <li class="active"><a href="./"> <div class="glyphicon glyphicon-hand-right"></div> <span class="sr-only ">(current)</span></a></li>
              <li><a href="#"> <i class="glyphicon glyphicon-heart-empty"></i> </a></li>
            </ul-->
          </div>
          <?php
         }
         ?>
          <!--/.nav-collapse -->
        </div><!--/.container-fluid -->
      </nav>
