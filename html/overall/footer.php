<footer class="page-footer font-small blue pt-4" style="background-color:black;color:white;">
  <div class="container-fluid bg-3 text-center">
    <h3>Contáctanos</h3>
    <div class="row">
    
      <div class="col-sm-4">
        <h4>Dirección y Tel</h4>
        <p>Ciudad de México</p>
        <p>Tlatelolco #3837</p>
        <p>Cp: 77777</p>
        <p>Tel: <span>55-158-335-777</span> </p>
        <p>Tel: <span>55-158-335-777</span> </p>
      </div>
        

      <div class="col-sm-4">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d481728.8046168815!2d-99.42381359739855!3d19.390519027263924!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85ce0026db097507%3A0x54061076265ee841!2sCiudad+de+M%C3%A9xico%2C+CDMX!5e0!3m2!1ses-419!2smx!4v1557870635594!5m2!1ses-419!2smx" width="400" height="200" frameborder="0" style="border:0" allowfullscreen></iframe>
      </div>
        
      <div class="col-sm-4">
        <h4>Redes Sociales</h4>
        <a href="https://www.facebook.com/augepuntocentral/"><i class="fab fa-facebook-square fa-3x"></i></a>
        <a href=""><i class="fab fa-instagram fa-3x"></i></a>
        <a href=""><i class="fab fa-youtube fa-3x"></i></a>
        <a href=""><i class="fab fa-twitter-square fa-3x"></i></a>
      </div>

    </div>
  </div> 
</footer>
<script src="css/js/jquery-3.2.1.min.js"></script>
<script src="css/bootstrap/js/bootstrap.min.js"></script>