
<!DOCTYPE html>
<html>
<head>
	<title>Auge Punto Central</title>
	<meta name="description" content="" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<link rel="stylesheet" type="text/css" href="css/197.css"/>
	<style>
		@import url(https://fonts.googleapis.com/css?family=Cinzel|Cormorant+Garamond:300|Nunito:300);
	.galbk-bot-base.spread.prev, .galbk-bot-base.spread.next { background-color:#fafafa; background-color:rgba(250,250,250,0.4); }.galbk-bot-base.spread.prev:hover, .galbk-bot-base.spread.next:hover { background-color:rgba(250,250,250,0.7); opacity:1; }.card_g .titulo.banda { background-color:#fafafa; background-color:rgba(250,250,250,0.5); }.wrapper-portada, .bottom-portada .linksEnter { background: #fafafa}body {color:#404040;font-family:Nunito, verdana;font-size:14px;background-color:#fafafa;padding:0 0 30px 0;margin:0;}@media screen and (max-width:1024px) {body {padding-top:30px;}}.wsetter {margin-left:auto;margin-right:auto;}.inGal .wsetter, .inGroupGal .wsetter {max-width:1300px;}.titSite, .logoSimple, .portadaHalf .logoPortadaF {color:#000000;font-family:Cinzel;font-size:37px;}.titSite a, .logoSimple a, .portadaHalf .logoPortadaF a {transition:color 0.5s ease;color:inherit;}.titSite span, .logoSimple span, .portadaHalf .logoPortadaF span {display:block;opacity:0.8;font-size:17px;margin-top:0px;}.titSite.titSiteSC, .logoSimple.titSiteSC, .portadaHalf .logoPortadaF.titSiteSC {display:none;}.portadaFull .titSite.titSiteSC, .portadaFull .logoSimple.titSiteSC, .portadaFull .portadaHalf .logoPortadaF.titSiteSC {display:block;}.portadaFull .titSite.titSitePC, .portadaFull .logoSimple.titSitePC, .portadaFull .portadaHalf .logoPortadaF.titSitePC {display:none;}.portadaFull .titSite,.portadaFull .wrapper-portada .logoSimple,.portadaMulti .wrapper-portada .logoSimple {color:#FFFFFF;}.logo {position:absolute;top:50px;left:70px;z-index:10;transition:all 0.3s ease;}.logo h1 {margin:0;}.logo .titSite img {max-width:180px;max-height:80px;margin-top:-20px;}@media screen and (max-width:1024px) {.logo {top:20px;left:20px;}}.tirador-wrapper-menu {position:fixed;right:65px;top:50px;cursor:pointer;z-index:201;transition:all 0.4s ease;padding:5px;}@media screen and (max-width:1024px) {.tirador-wrapper-menu {right:30px;top:35px;}}.galeriaSBGhover .tirador-wrapper-menu {left:-9999em;}.tirador-wrapper-menu svg {fill:currentColor;width:25px;height:25px;}.hero-img-body .tirador-wrapper-menu svg.opener {fill:#FFF;}.tirador-wrapper-menu svg.closer {display:none;width:20px;height:20px;fill:#fafafa;}@media screen and (max-width:768px) {.tirador-wrapper-menu svg.closer {width:15px;height:15px;}}@media screen and (min-width:768px) {.hero-img-body .tirador-wrapper-menu.scrolled {top:25px;right:40px;}}.hero-img-body .tirador-wrapper-menu.scrolled svg.opener {width:20px;height:20px;fill:#404040;}.tirador-wrapper-menu.open {right:90px;}.tirador-wrapper-menu.open svg.opener {display:none;}.tirador-wrapper-menu.open svg.closer {display:inline-block;}@media screen and (max-width:1024px) {.tirador-wrapper-menu.open {top:20px;right:15px;padding-left:30px;}}.wrapper-menu {position:fixed;left:50%;right:0;top:0;bottom:0;display:flex;flex-direction:column;justify-content:space-between;color:#fafafa;background:#000;overflow-x:hidden;overflow-y:auto;transition:all 0.5s ease;transform:translate3d(100%, 0, 0);z-index:200;}.wrapper-menu.visible {transform:translate3d(0, 0, 0);}@media screen and (max-width:768px) {.wrapper-menu {left:0;}}#menu {color:inherit;font-size:20px;font-family:Cormorant Garamond;text-align:center;overflow-y:auto;padding:20px 0;}#menu ul {font-size:2em;list-style:none;margin:0;padding:0;position:relative;}#menu ul li {display:block;padding:0;margin:0;}#menu a {display:block;padding:0.2em 0;color:inherit;opacity:1;transition:opacity 0.3s;}#menu a:hover {opacity:0.7;}#menu:hover {opacity:1;}@media screen and (max-width:1024px) {#menu {font-size:12px;}}.velo-bg-menu {position:fixed;left:0;width:50%;top:0;bottom:0;background-color:#fafafa;opacity:0.85;z-index:200;transition:all 0.5s ease;transform:translate3d(-100%, 0, 0);}@media screen and (max-width:768px) {.velo-bg-menu {left:-50%;}}.velo-bg-menu.menu-open {transform:translate3d(0, 0, 0);}#tit-pagina {position:fixed;display:block;left:40px;bottom:3em;width:1em;text-align:center;line-height:3em;opacity:0.6;word-break:break-all;font-size:10px;text-transform:uppercase;font-family:Trebuchet MS;font-size:10px;color:#000000;z-index:100;}#tit-pagina h2 {font-weight:normal;margin:0;padding:0;}.hero-img-body #tit-pagina {position:absolute;top:50px;bottom:auto;left:10%;width:80%;text-align:center;letter-spacing:0.05em;text-transform:none;color:#FFF;font-size:3.5em;opacity:1;}@media screen and (max-width:1024px) {.hero-img-body #tit-pagina {font-size:2em;word-break:initial;line-height:2em;top:100px;}}@media screen and (max-width:768px) {.hero-img-body #tit-pagina {font-size:1.5em;}}#container {margin:200px auto 50px;min-height:400px;}.hero-img-body #container {margin:0 auto;min-height:200vh;}#middle, #contents {position:relative;}#middle {margin-top:170px;}.hero-img-body #middle {margin-top:0;}@media screen and (min-width:1024px) and (max-width:1300px) {.introPosicionBottom, .introPosicionTop {padding:0 1em;}}#linksFoot {line-height:30px;text-align:center;margin-bottom:20px;}#linksFoot a {color:inherit;}#linksFoot #idiomas {margin:0 0 1em 0;text-align:inherit;float:none;}#linksFoot .siguenosFA {float:none;}.textFoot {margin:50px 0 50px 15px;font-size:0.8em;}.galeriaSBG .textFoot, .fotogrande .textFoot, .portadaFull .textFoot {position:fixed;top:-9999em;}.galbk-titulo {text-transform:uppercase;font-size:0.75em;letter-spacing:0.3em;}.galbk-titulo a {letter-spacing:0.1em;}a {color:#404040;text-decoration:none;}input, textarea {border-color:#404040;color:#404040;font-family:Nunito, verdana;font-size:14px;}.bodyBKblog #container, .bodyBKblog #access .menu-header, .bodyBKblog div.menu, .bodyBKblog #colophon, .bodyBKblog #branding, .bodyBKblog #main, .bodyBKblog #wrapper {width:100%;}.bodyBKblog #tit-pagina h2 {display:none;}.portadaFull .titSite, .portadaFull .tirador-wrapper-menu {color:#FFF;}.portadaFull .tirador-wrapper-menu {top:70px;}.portadaFull .tirador-wrapper-menu.open {top:25px;right:40px;}@media screen and (max-width:768px) {.portadaFull .tirador-wrapper-menu {left:auto;top:50px;}}.portadaFull.portadaHalf .tirador-wrapper-menu {color:#333;}.portadaFull .wrapperPortadaF, .portadaFull .tiradorMenuPortadaF, .portadaFull .logoPortadaF, .portadaFull .tiradorCart, .portadaFull #tit-pagina {display:none;}.portadaFull #menu {display:block;}.portadaFull .logo {display:block;width:50vw;left:5vw;}@media screen and (max-width:1024px) {.portadaFull .logo {top:50px;}}.portadaFull #linksFoot {display:block;}.portadaSlogan .logo {left:10%;}.portadaSlogan .menuPortadaF {display:none;}@media screen and (max-width:768px) {.portadaSlogan .tirador-wrapper-menu {display:none;}}.tirador-hero {display:flex;justify-content:center;align-items:center;position:absolute;bottom:50px;left:50%;margin-left:-25px;width:50px;height:50px;background:rgba(255, 255, 255, 0.6);color:#555;border-radius:50%;cursor:pointer;z-index:15;}.tirador-hero svg {fill:currentColor;height:20px;width:20px;}@media screen and (max-width:768px) {.tirador-hero {bottom:100px;}}@keyframes blinker {50% {opacity:0;}}.blinker-bk {animation:blinker 1.75s linear infinite;}.tirador-groupgal {left:60px;opacity:1;top:25px;text-transform:lowercase;opacity:0.6;transition:color 0.5s ease;z-index:100;transition:all 0.2s ease;}.tirador-groupgal.scrolled {font-size:1em;opacity:1;}@media screen and (min-width:768px) {.tirador-groupgal.scrolled {right:100px;}}.tirador-groupgal .arrleft, .tirador-groupgal .cruz {display:none;}.tirador-groupgal span {display:block;font-size:0.9em;}.hero-img-body .tirador-groupgal {color:#FFF;}.hero-img-body .scrolled .tirador-groupgal {color:#404040;}.tirador-groupgal.no-group {display:none;}.hero-img-body .titSite {display:none;}.hero-img-body.galeriaSBG {padding-bottom:0;}.hero-img-body #container {margin-top:0;}.hero-img-body .contenidosIntroLateral {margin-right:0;}.hero-img-body #bottom {display:none !important;}.continua_g {margin-top:0;}.infiniteContainerV {margin-bottom:120px;}.infiniteContainerV .tituloInfiniteV {text-transform:uppercase;font-size:0.85em;}.bannerTrial {left:25%;}.logoBlogMobile {display:none !important;}@media screen and (max-width:1024px) {.bodyBKblog .tirador-wrapper-menu {display:none;}}.portadaWideFull .soundpf-on {top:auto;bottom:50px;}@media screen and (max-width:48em) { .bk-touch body { font-size:17px } }</style>
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
	<script type="text/javascript">var uriDomain = 'https://ac.bluekea.com/demo-gion/';var uriUserSSL = 'https://ac.bluekea.com/demo-gion/';var uriUserSSLlang = 'https://ac.bluekea.com/demo-gion/';var uriBase = 'https://res.bluekea.com/account/';var uriBaseCDN = 'https://d3l48pmeh9oyts.cloudfront.net/';var langBK = '';var __codePortada = 'inicio';var uriCurrent = 'https://ac.bluekea.com/demo-gion/';var uriSChar = false;</script><script type="text/javascript" src="https://d3l48pmeh9oyts.cloudfront.net/min2/a_js__canvas__default/197"></script>

	<script type="text/javascript">
		$(function() { menuSBG = $('#menu').sbgMenu({cssMenu:'',colorAct:'#7c9ab0', colorNor:'#fafafa', colorHov:'#7c9ab0', primerCode:'inicio', blog: '0'}); })
	</script>
	<script type="text/javascript">

		function ajaxLoadPagina(_code) 
		{

			$('html').addClass('bk-loading-ajax');

			_code = typeof _code == 'undefined' ? '' : _code;

			$.ajax({
				url: 'https://ac.bluekea.com/demo-gion/index.php?ajax=1&c=' + _code,
				dataType: 'json',
				success: function(data, textStatus) {

					$(document).unbind("keydown.key-galbk");

					$('#controles').html('').hide();
					$('#caption, .galbk-elm').html('').hide();
					$('.compartirFoto').remove();

					setTitulo(data.tituloSEO);
					setCanonical(data.canonical);
					stopSpinner();

					$('body').removeClass().addClass(data.addClasses);

					$('.idiomasBK').html(data.idiomas);

					loadTituloPagina(data.targetTitulo, data.titulo);

					$('#' + data.targetContenidos).html(data.contenidos);
					$('#js_contents').html(data.contenidosJS);

					codigoJS();
					resumeMusic();
					sendAlert();

				},
				error: function(xhr, textStatus, errorThrown) {}
			});
		}


	var avisoCookBK = false;</script>
	<meta property="og:url" content=""/>
	<meta property="og:image" content="http://image-server.bluekea.com/imgserver/demo-gion/1200/0/-----/36101_921.jpg"/>
	<meta property="og:type" content="website" /><meta property="og:image:width" content="1300" /> <meta property="og:image:height" content="758" /> <meta property="og:title" content="Bodas" /> </head>
	<body class="inGal uniform inGalGroup hero-img-body">
		<a class="tiradorCart" href="#" onclick="showCartBK();return false;"><svg class=""><use xlink:href="#i-shopping-cart" /></svg></a><div id="wrapperCartBK"></div><div id="innerBody">


			<div class="velo-bg-menu menu-triggered"></div>



			<div id="tit-pagina" class="set-scroll">
				<h2><span id="sbg_titulo">Portadas Auge</span></h2>
			</div>



			<div id="container-md" class="wsetter set-scroll">
				<div id="middle">	
					<div id="contents" class="fusible">
						<div id="sbg_contents">
							<div class="hero-img-container hero-img-container83764 oculto-pos" data-bk-src="https://d2t54f3e471ia1.cloudfront.net/demo-gion/multimedia/galerias/fotos/36101_921.jpg">
								<img src="https://d2t54f3e471ia1.cloudfront.net/demo-gion/multimedia/galerias/fotos/36101_921.jpg"></div>
								<div class="hero-img hero-img83764">
									<div class="hero-img-bg">

									</div>
									<div class="tirador-hero">
										<svg class="blinker-bk ">
											<use xlink:href="#i-long-arrow-down" /></svg>
										</div>

										<div class="spinnerElementIndep la-ball-clip-rotate loader-bk">
											<div>

											</div>
										</div>
									</div>
									<div class="bottom-hero set-scroll">

									</div>
									<div class="wrapper-galbk">
										<div class="galbk galbk_transslide galbk_hover galbk_strips galbk_83764 " data-bk-numpaginas="2">
											<div class="lineBot pre-hover">

											</div>
											<svg class="pre-hover galbk-bot-base prev ">
												<use xlink:href="#i-angle-left-btm" /></svg>
												<svg class="pre-hover galbk-bot-base next ">
													<use xlink:href="#i-angle-right-btm" /></svg>
													<svg class="pre-hover galbk-bot-base play ">
														<use xlink:href="#i-play" /></svg>
														<svg class="pre-hover galbk-bot-base pause ">
															<use xlink:href="#i-pause" />
														</svg>
														<svg class="pre-hover galbk-bot-base close ">
															<use xlink:href="#i-times-btm" />
														</svg>
														<div class="shieldBK"></div>
													</div>
													<div class="galbk-elm galbk-titulo galbk-titulo-83764 galbk-titulo-hover"></div>
												</div>
												<div id="galeria" data-bk-altomax="1950" data-bk-ratio="1.5" data-bk-altomin="600" class="nojs tipo_strips pure-g galeria_83764">
													<div class="pure-u-1-2 pure-u-sm-1-3 pagina1  bkth bkth0">
														<a  data-bk-titulo="" href="https://d2t54f3e471ia1.cloudfront.net/demo-gion/multimedia/galerias/fotos/36096_753.jpg"  data-bk-ratio = "0.667" class="thumb thumb_2124334" data-bk-index="0">
															<div class="thumb_bk">
																<div class="thumb_placeholder" style="padding-bottom: 70%">
																	<img data-bk-id="2124334"  src = "https://d16q99dnji1cly.cloudfront.net/imgserver/demo-gion/5035/1/-----/36096_753.jpg">
																	<div class="shieldTh">

																	</div>
																</div>
															</div>
														</a>
													</div>
													<div class="pure-u-1-2 pure-u-sm-1-3 pagina1  bkth bkth1">
														<a  data-bk-titulo="" href="https://d2t54f3e471ia1.cloudfront.net/demo-gion/multimedia/galerias/fotos/36097_290.jpg"  data-bk-ratio = "0.667" class="thumb thumb_2124335" data-bk-index="1">
															<div class="thumb_bk">
																<div class="thumb_placeholder" style="padding-bottom: 70%">
																	<img data-bk-id="2124335"  src = "https://d16q99dnji1cly.cloudfront.net/imgserver/demo-gion/5035/1/-----/36097_290.jpg">
																	<div class="shieldTh">

																	</div>
																</div>
															</div>
														</a>
													</div>
													<div class="pure-u-1-2 pure-u-sm-1-3 pagina1  bkth bkth2"><a  data-bk-titulo="" href="https://d2t54f3e471ia1.cloudfront.net/demo-gion/multimedia/galerias/fotos/36098_507.jpg"  data-bk-ratio = "0.667" class="thumb thumb_2124336" data-bk-index="2">
														<div class="thumb_bk">
															<div class="thumb_placeholder" style="padding-bottom: 70%">
																<img data-bk-id="2124336"  src = "https://d16q99dnji1cly.cloudfront.net/imgserver/demo-gion/5035/1/-----/36098_507.jpg">
																<div class="shieldTh">

																</div>
															</div>
														</div>
													</a>
												</div>
												<div class="pure-u-1-2 pure-u-sm-1-3 pagina1  bkth bkth3">
													<a  data-bk-titulo="" href="https://d2t54f3e471ia1.cloudfront.net/demo-gion/multimedia/galerias/fotos/36101_921.jpg"  data-bk-ratio = "1.715" class="thumb thumb_2124330" data-bk-index="3">
														<div class="thumb_bk">
															<div class="thumb_placeholder" style="padding-bottom: 70%">
																<img data-bk-id="2124330"  src = "https://d16q99dnji1cly.cloudfront.net/imgserver/demo-gion/5035/1/-----/36101_921.jpg">
																<div class="shieldTh">

																</div>
															</div>
														</div>
													</a>
												</div>
												<div class="pure-u-1-2 pure-u-sm-1-3 pagina1  bkth bkth4">
													<a  data-bk-titulo="" href="https://d2t54f3e471ia1.cloudfront.net/demo-gion/multimedia/galerias/fotos/36102_935.jpg"  data-bk-ratio = "1.498" class="thumb thumb_2124331" data-bk-index="4">
														<div class="thumb_bk">
															<div class="thumb_placeholder" style="padding-bottom: 70%">
																<img data-bk-id="2124331"  src = "https://d16q99dnji1cly.cloudfront.net/imgserver/demo-gion/5035/1/-----/36102_935.jpg">
																<div class="shieldTh">

																</div>
															</div>
														</div>
													</a>
												</div>
												<div class="pure-u-1-2 pure-u-sm-1-3 pagina1  bkth bkth5"><a  data-bk-titulo="" href="https://d2t54f3e471ia1.cloudfront.net/demo-gion/multimedia/galerias/fotos/36103_291.jpg"  data-bk-ratio = "1.499" class="thumb thumb_2124332" data-bk-index="5">
													<div class="thumb_bk">
														<div class="thumb_placeholder" style="padding-bottom: 70%">
															<img data-bk-id="2124332"  src = "https://d16q99dnji1cly.cloudfront.net/imgserver/demo-gion/5035/1/-----/36103_291.jpg">
															<div class="shieldTh">

															</div>
														</div>
													</div>
												</a>
											</div><div class="pure-u-1-2 pure-u-sm-1-3 pagina1  bkth bkth6">
												<a  data-bk-titulo="" href="https://d2t54f3e471ia1.cloudfront.net/demo-gion/multimedia/galerias/fotos/36104_494.jpg"  data-bk-ratio = "1.499" class="thumb thumb_2124329" data-bk-index="6">
													<div class="thumb_bk">
														<div class="thumb_placeholder" style="padding-bottom: 70%">
															<img data-bk-id="2124329"  src = "https://d16q99dnji1cly.cloudfront.net/imgserver/demo-gion/5035/1/-----/36104_494.jpg"><div class="shieldTh">

															</div>
														</div>
													</div>
												</a>
											</div>
											<div class="pure-u-1-2 pure-u-sm-1-3 pagina1  bkth bkth7">
												<a  data-bk-titulo="" href="https://d2t54f3e471ia1.cloudfront.net/demo-gion/multimedia/galerias/fotos/36105_815.jpg"  data-bk-ratio = "1.366" class="thumb thumb_2124328" data-bk-index="7">
													<div class="thumb_bk">
														<div class="thumb_placeholder" style="padding-bottom: 70%">
															<img data-bk-id="2124328"  src = "https://d16q99dnji1cly.cloudfront.net/imgserver/demo-gion/5035/1/-----/36105_815.jpg"><div class="shieldTh">

															</div>
														</div>
													</div>
												</a>
											</div>
											<div class="pure-u-1-2 pure-u-sm-1-3 pagina1  bkth bkth8">
												<div class="boton-pag pagina1000">
													<svg class="">
														<use xlink:href="#i-angle-right-btm" />
													</svg>
												</div>
												<a  data-bk-titulo="" href="https://d2t54f3e471ia1.cloudfront.net/demo-gion/multimedia/galerias/fotos/36107_495.jpg" data-bk-ret=https://d2t54f3e471ia1.cloudfront.net/demo-gion/multimedia/galerias/fotos/36107_495.jpg data-bk-ratio = "1.475" class="thumb thumb_2124333" data-bk-index="8">
													<div class="thumb_bk">
														<div class="thumb_placeholder" style="padding-bottom: 70%">
															<img data-bk-id="2124333"  src = "https://d16q99dnji1cly.cloudfront.net/imgserver/demo-gion/5035/1/-----/36107_495.jpg"><div class="shieldTh">

															</div>
														</div>
													</div>
												</a>
											</div>
											<div class="pure-u-1-2 pure-u-sm-1-3 pagina2 thumbHidden bkth bkth9">
												<div class="boton-pag pagina0">
													<svg class="">
														<use xlink:href="#i-angle-left-btm" />
													</svg>
												</div>
												<a  data-bk-titulo="" href="https://d2t54f3e471ia1.cloudfront.net/demo-gion/multimedia/galerias/fotos/36120_810.jpg" data-bk-ret=https://d2t54f3e471ia1.cloudfront.net/demo-gion/multimedia/galerias/fotos/36120_810.jpg data-bk-ratio = "1.517" class="thumb thumb_2124326" data-bk-index="9">
													<div class="thumb_bk">
														<div class="thumb_placeholder" style="padding-bottom: 70%">
															<img data-bk-id="2124326"  data-src = "https://d16q99dnji1cly.cloudfront.net/imgserver/demo-gion/5035/1/-----/36120_810.jpg"><div class="shieldTh">

															</div>
														</div>
													</div>
												</a>
											</div>
											<div class="pure-u-1-2 pure-u-sm-1-3 pagina2 thumbHidden bkth bkth10"><a  data-bk-titulo="" href="https://d2t54f3e471ia1.cloudfront.net/demo-gion/multimedia/galerias/fotos/36121_835.jpg" data-bk-ret=https://d2t54f3e471ia1.cloudfront.net/demo-gion/multimedia/galerias/fotos/36121_835.jpg data-bk-ratio = "1.417" class="thumb thumb_2124327" data-bk-index="10"><div class="thumb_bk"><div class="thumb_placeholder" style="padding-bottom: 70%"><img data-bk-id="2124327"  data-src = "https://d16q99dnji1cly.cloudfront.net/imgserver/demo-gion/5035/1/-----/36121_835.jpg">
												<div class="shieldTh"></div>
											</div>
										</div>
									</a>
								</div>
							</div>
						</div>
						<div id="js_contents">

						</div>			
						<div id="caption">

						</div>

					</div>
				</div>

				<div class="textFoot fusible"></div>

			</div>

			<div id="copyright">&copy; John Johnson</div>
			<script type="text/javascript">var codeGA = ''</script>
		</div>

		<div class="wrapperPortadaF blanco"><div class="logoPortadaF"><a href="galerias" onclick = "pushStateBK('galerias'); return false;">John Johnson<span class="subTitulo">FOTO&VIDEO</span></a></div><div class="menuPortadaH blanco" style="font-family:Cormorant Garamond; text-transform: uppercase; font-size:17px;"><ul class="l1">
			<li><a id="link_galerias" class="l1 tipo_g" href="https://ac.bluekea.com/demo-gion/galerias" rel="galerias">Galerías</a></li>
			<li><a id="link_sobre-mi" class="l1 tipo_t" href="https://ac.bluekea.com/demo-gion/sobre-mi" rel="sobre-mi">Sobre mí</a></li>
			<li><a id="link_video" class="l1 tipo_v" href="https://ac.bluekea.com/demo-gion/video" rel="video">Vídeo</a></li>
			<li><a id="link_contacto" class="l1 tipo_f" href="https://ac.bluekea.com/demo-gion/contacto" rel="contacto">Contacto</a></li>
		</ul></div></div><div class="pase-fondo-container"></div>
		<script>
			function sbgIsHDdevice() {
				var mediaQuery = "(-webkit-min-device-pixel-ratio: 1.5),\
				(min--moz-device-pixel-ratio: 1.5),\
				(-o-min-device-pixel-ratio: 3/2),\
				(min-resolution: 1.5dppx)";
				if (window.devicePixelRatio > 1)
					return true;
				if (window.matchMedia && window.matchMedia(mediaQuery).matches)
					return true;
				return false;
			}
			if (sbgIsHDdevice()) {
				document.cookie="bluekea_hddevice=ok";
			}
		</script>

		<div class="auxFusible"></div>
		<div class="info-bk-foot"><span id="textoPie">contenidos © John Johnson</span><span id="linkPB">creado en <a href="https://bluekea.com" target="_blank" rel="nofollow">Bluekea</a></span><a href="#" class="closeinfobk" onclick="toggleInfoPie(); return false;"><svg class=""><use xlink:href="#i-long-arrow-down" /></svg></a> </div>
		<script>
			$(function() {
				fadeInContent();;$('.galeria_83764').galBluekea({ idPagina: 83764, tipoGal: 'strips', tiempoFoto: 5000, loopPase: 0, transicion: 'fadeslide',
					tiempoTransicion: 600, autoStart: 0, designAsFrame: 0, presentacion: 'hover', 
					tipoMarco: 'none', dimMarco: '0', colorMarco: '333333', sombra: 'off', colorFondo:'000000', alphaFondo: '0.96',
					cartOn: 'off', lblComprar: 'Comprar', lblCompartir: 'Compartir', lblVolver: 'Galeria', posBotonVenta: 'titulo', 
					'carousel': 0, 'compartirImg': 0,
					'fotoGrande': 0, 'noredim': true, showCopyRight: '1'
				});;esperarImgLoaded(83764);activarFadeHoverImg(83764);;lanzarHero(83764);;;$('.botonesMusic').show();;makeIframeResponsive('#sbg_contents', '');;activarMenuComun();menuSBG.loadTitulo('Bodas');;setScroll(200);	})
			sendAlert();
		</script>
		<svg width="0" height="0" class="oculto-pos">
			<symbol id="i-angle-left-btm" viewBox="0 0 576 1008"><g transform="matrix(1 0 0 -1 0 864)">
				<path d="M18 360l504 504l54 -54l-450 -450l450 -450l-54 -54z"/>
			</g></symbol><symbol id="i-angle-right-btm" viewBox="0 0 576 1008"><g transform="matrix(1 0 0 -1 0 864)">
				<path d="M54 864l504 -504l-504 -504l-54 54l450 450l-450 450z"/>
			</g></symbol><symbol id="i-arrow-right" viewBox="0 0 1008 1008"><g transform="matrix(1 0 0 -1 0 840)">
				<path d="M167 294v82h504l-230 233l59 59l333 -333l-333 -333l-59 59l230 233h-504z"/>
			</g></symbol><symbol id="i-bars" viewBox="0 -50 864 1008"><path d="M864 684v-72c0 -20 -16 -36 -36 -36h-792c-20 0 -36 16 -36 36v72c0 20 16 36 36 36h792c20 0 36 -16 36 -36zM864 396v-72c0 -20 -16 -36 -36 -36h-792c-20 0 -36 16 -36 36v72c0 20 16 36 36 36h792c20 0 36 -16 36 -36zM864 108v-72c0 -20 -16 -36 -36 -36h-792 c-20 0 -36 16 -36 36v72c0 20 16 36 36 36h792c20 0 36 -16 36 -36z"/></symbol><symbol id="i-bars-btm" viewBox="0 0 1008 1008"><g transform="matrix(1 0 0 -1 0 864)">
				<path d="M1008 432v-72h-1008v72h1008zM1008 72v-72h-1008v72h1008zM1008 792v-72h-1008v72h1008z"/>
			</g></symbol><symbol id="i-circle-o" viewBox="0 0 864 1008"><g transform="matrix(1 0 0 -1 0 864)">
				<path d="M864 360c0 -238 -193 -432 -432 -432s-432 194 -432 432s193 432 432 432v0c239 0 432 -194 432 -432zM432 666c-169 0 -306 -137 -306 -306s137 -306 306 -306s306 137 306 306s-137 306 -306 306z"/>
			</g></symbol><symbol id="i-copyright" viewBox="0 0 864 1008"><g transform="matrix(1 0 0 -1 0 864)">
				<path d="M864 360c0 -238 -193 -432 -432 -432s-432 194 -432 432s193 432 432 432v0c239 0 432 -194 432 -432zM432 720c-199 0 -360 -161 -360 -360s161 -360 360 -360s360 161 360 360s-161 360 -360 360zM647 260v-61c0 -80 -127 -109 -206 -109c-154 0 -270 118 -270 273 c0 152 115 267 267 267c56 0 202 -20 202 -109v-61c0 -6 -4 -9 -9 -9h-67c-5 0 -9 4 -9 9v39c0 35 -68 52 -114 52c-105 0 -178 -76 -178 -185c0 -113 76 -196 182 -196c41 0 118 15 118 51v39c0 5 3 9 8 9h67c4 0 9 -4 9 -9z"/>
			</g></symbol><symbol id="i-envelope" viewBox="0 0 1008 1008"><g transform="matrix(1 0 0 -1 0 864)">
				<path d="M1008 630c0 -63 -47 -120 -96 -154c-88 -61 -176 -122 -263 -183c-37 -25 -99 -77 -144 -77h-1h-1c-45 0 -107 52 -144 77c-87 61 -175 123 -262 183c-40 27 -97 90 -97 142c0 55 30 102 90 102h828c49 0 90 -40 90 -90zM1008 465v-447c0 -50 -40 -90 -90 -90h-828 c-50 0 -90 40 -90 90v447c17 -19 36 -35 57 -49c93 -64 188 -127 279 -194c48 -35 106 -78 167 -78h1h1c61 0 119 43 167 78c91 66 186 130 280 194c20 14 39 30 56 49z"/>
			</g></symbol><symbol id="i-facebook" viewBox="0 0 864 1008"><g transform="matrix(1 0 0 -1 0 864)">
				<path d="M162 792h540c89 0 162 -73 162 -162v-540c0 -89 -73 -162 -162 -162h-106v335h112l17 130h-129v83c0 38 10 63 65 63l69 1v116c-12 2 -53 6 -101 6c-99 0 -168 -61 -168 -173v-96h-112v-130h112v-335h-299c-89 0 -162 73 -162 162v540c0 89 73 162 162 162z"/>
			</g></symbol><symbol id="i-instagram" viewBox="0 0 864 1008"><g transform="matrix(1 0 0 -1 0 864)">
				<path d="M864 360c0 -60 0 -119 -3 -178c-3 -69 -19 -131 -70 -181c-50 -51 -112 -67 -181 -70c-59 -3 -118 -3 -178 -3s-119 0 -178 3c-69 3 -131 19 -181 70c-51 50 -67 112 -70 181c-3 59 -3 118 -3 178s0 119 3 178c3 69 19 131 70 181c50 51 112 67 181 70c59 3 118 3 178 3 s119 0 178 -3c69 -3 131 -19 181 -70c51 -50 67 -112 70 -181c3 -59 3 -118 3 -178zM432 714c-63 0 -198 5 -255 -17c-20 -8 -34 -18 -49 -33s-25 -29 -33 -49c-22 -57 -17 -192 -17 -255s-5 -198 17 -255c8 -20 18 -34 33 -49s29 -25 49 -33c57 -22 192 -17 255 -17 s198 -5 255 17c20 8 34 18 49 33c16 15 25 29 33 49c22 57 17 192 17 255s5 198 -17 255c-8 20 -17 34 -33 49c-15 15 -29 25 -49 33c-57 22 -192 17 -255 17zM714 591c0 -29 -23 -52 -51 -52c-29 0 -52 23 -52 52c0 28 23 51 52 51c28 0 51 -23 51 -51zM654 360 c0 -123 -99 -222 -222 -222s-222 99 -222 222s99 222 222 222s222 -99 222 -222zM576 360c0 79 -65 144 -144 144s-144 -65 -144 -144s65 -144 144 -144s144 65 144 144z"/>
			</g></symbol><symbol id="i-long-arrow-down" viewBox="0 0 576 1008"><g transform="matrix(1 0 0 -1 0 864)">
				<path d="M288 -144l-252 252l54 54l162 -162v864h72v-864l162 162l54 -54z"/>
			</g></symbol><symbol id="i-long-arrow-up" viewBox="0 0 576 1008"><g transform="matrix(1 0 0 -1 0 864)">
				<path d="M36 612l252 252l252 -252l-54 -54l-162 162v-864h-72v864l-162 -162z"/>
			</g></symbol><symbol id="i-menu" viewBox="0 0 1008 1008"><g transform="matrix(1 0 0 -1 0 819)">
				<path d="M1008 567h-1008v126h1008v-126zM1008 193h-1008v126h1008v-126zM1008 -185h-1008v126h1008v-126z"/>
			</g></symbol><symbol id="i-pause" viewBox="0 0 576 1008"><g transform="matrix(1 0 0 -1 0 864)">
				<path d="M576 864v-1008h-144v1008h144zM144 864v-1008h-144v1008h144z"/>
			</g></symbol><symbol id="i-pinterest" viewBox="0 0 864 1008"><g transform="matrix(1 0 0 -1 0 864)">
				<path d="M864 360c0 -238 -194 -432 -432 -432c-43 0 -83 6 -123 18c17 26 36 59 44 92c0 0 5 19 31 119c14 -29 58 -54 105 -54c139 0 233 126 233 296c0 128 -108 248 -274 248c-204 0 -308 -148 -308 -270c0 -74 28 -141 88 -166c10 -3 19 1 22 12c2 7 7 26 9 34 c3 11 1 15 -6 24c-18 21 -29 47 -29 85c0 109 81 207 213 207c115 0 180 -71 180 -166c0 -124 -56 -229 -138 -229c-45 0 -79 37 -68 83c13 55 38 114 38 153c0 35 -19 65 -58 65c-46 0 -83 -48 -83 -112c0 0 0 -41 14 -68c-47 -201 -56 -235 -56 -235 c-8 -33 -8 -70 -7 -100c-153 67 -259 219 -259 396c0 238 193 432 432 432s432 -194 432 -432z"/>
			</g></symbol><symbol id="i-play" viewBox="0 0 792 1008"><g transform="matrix(1 0 0 -1 0 864)">
				<path d="M779 343l-747 -416c-18 -9 -32 -1 -32 19v828c0 20 14 28 32 19l747 -416c17 -9 17 -25 0 -34z"/>
			</g></symbol><symbol id="i-quote-entypo" viewBox="0 0 1008 1008"><g transform="matrix(1 0 0 -1 0 864)">
				<path d="M692 690l1 -3c85 0 143 -38 174 -115c29 -71 30 -155 2 -253s-76 -175 -144 -231c-55 -45 -115 -67 -180 -67v71c66 0 122 29 167 87c36 45 53 89 51 132c-2 49 -26 73 -71 73c-41 0 -76 15 -105 45s-43 66 -43 108s14 78 43 108s64 45 105 45zM268 693l1 -3 c85 0 143 -38 174 -115c29 -71 30 -155 2 -253s-76 -175 -144 -231c-55 -45 -115 -67 -180 -67v71c66 0 122 29 167 87c36 45 53 89 51 132c-2 49 -26 73 -71 73c-41 0 -76 15 -105 45s-43 66 -43 108s14 78 43 108s64 45 105 45z"/>
			</g></symbol><symbol id="i-times-btm" viewBox="0 0 1008 1008"><g transform="matrix(1 0 0 -1 0 864)">
				<path d="M1008 810l-450 -450l450 -450l-54 -54l-450 450l-450 -450l-54 54l450 450l-450 450l54 54l450 -450l450 450z"/>
			</g></symbol><symbol id="i-twitter" viewBox="0 0 936 1008"><g transform="matrix(1 0 0 -1 0 864)">
				<path d="M911 634c-25 -35 -55 -68 -91 -93c1 -8 1 -16 1 -24c0 -240 -183 -517 -517 -517c-103 0 -199 30 -279 82c14 -2 28 -3 44 -3c85 0 163 29 225 78c-80 2 -147 54 -170 126c12 -2 23 -3 35 -3c16 0 32 2 47 6c-83 17 -145 90 -145 179v2c24 -14 52 -22 82 -23 c-49 32 -81 88 -81 151c0 34 9 65 25 92c89 -110 224 -182 374 -190c-3 13 -4 27 -4 41c0 100 81 182 181 182c53 0 100 -22 133 -57c41 8 81 23 115 44c-13 -43 -42 -78 -79 -101c36 4 72 14 104 28z"/>
			</g></symbol></svg></body>
			</html>