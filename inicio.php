
<!DOCTYPE html>
<html>
<head>
  <title>Auge Punto Central</title>
  <meta name="description" content="" />
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <link rel="stylesheet" type="text/css" href="css/197.css"/>
  <style>
    @import url(https://fonts.googleapis.com/css?family=Bellefair|Raleway:300,700);
    .galbk-bot-base.spread.prev, .galbk-bot-base.spread.next{ 
      background-color:#FFFFFF; background-color:rgba(255,255,255,0.4);
    }
    .galbk-bot-base.spread.prev:hover, .galbk-bot-base.spread.next:hover { 
      background-color:rgba(255,255,255,0.7); opacity:1;
    }
    .card_g .titulo.banda { background-color:#FFFFFF; background-color:rgba(255,255,255,0.5); }
    .wrapper-portada, .bottom-portada .linksEnter { background: #FFFFFF}* {margin:0;padding:0;}
    body {color:#474747;font-family:Bellefair, Sans-Serif, Verdana;font-size:18px;margin-bottom:50px;background-color:#FFFFFF;}
    .wshort-bk .wsetter {max-width:1200px;}
    input, textarea {color:#474747;
    }
    #container, .intro-lateral.galeriaSBG 
    #container {position:relative;margin:0 auto;margin-top:170px;padding:0 20px 50px;}
    .portada .wrapper-portada.multi .galerias-portada {max-width:1300px;}
    .portada-mmenu {font-family:Raleway;}@media screen and (min-width:80em) {
      .galeriaSBG #container {
        max-width:100%;padding:0 0 0 10%;
      }
      .intro-lateral.galeriaSBG #container {max-width:1200px;}
      .galbk .titulo {text-align:left;}
      .galbk_hover .titulo {text-align:center;}}
      .wrapperBanda {position:fixed;top:0;left:0;width:100%;transition:all 0.6s ease-in-out;z-index:10;background-color:#FFFFFF;background-color:rgba(255,255,255,.8);}.lineTitulos {position:fixed;top:38px;left:0;width:100%;height:1px;z-index:11;border-bottom:1px solid transparent;border-color:#242424;opacity:0.15;}
      #logo {display:none;height:100px;width:100%;margin-bottom:30px;transition:all 0.3s ease;}
      #logo h1 {width:100%;height:100%;}
      #logo h1 a {display:block;height:100%;width:100%;text-indent:-2000em;text-decoration:none;z-index:1000;cursor:default;background-size:contain;}
      #menu {display:inline-block;z-index:5;}
      #menu ul {padding:0;margin:0;text-transform:uppercase;letter-spacing:0.05em;}
      #menu li {height:100%;display:inline-block;color:#242424;}
      #menu a {height:100%;line-height:40px;font-size:12px;font-family:Raleway, verdana;display:block;padding:0 20px;color:inherit;transition:all 0.3s ease;}
      #menu a.current, #menu a:hover {opacity:0.5;}
      #menu li {position:relative;}
      #menu ul.l2 {position:absolute;left:-20px;top:100%;margin:0;padding:15px 20px 20px;text-align:left;background:#FFF ;background-color:#FFFFFF;border-bottom:1px solid rgba(125, 125, 125, 0.4);min-width:120px;z-index:10;visibility:hidden;opacity:0;transition:visibility 0.2s, opacity 0.2s linear;}
      #menu ul.l2 li {margin:0;padding:0;display:block;}
      #menu ul.l2 a {padding:7px 0;line-height:1.25em;padding-left:15px;}
      #menu li:hover ul.l2 {visibility:visible;opacity:1;}
      #menu ul.l2 {left:0;}
      .wrapperTitulos {position:fixed;left:0;top:0;height:38px;width:100%;padding-left:30px;background-color:#FFFFFF;background-color:rgba(255,255,255,.5);border-bottom:1px solid #FFFFFF;border-bottom:1px solid rgba(36,36,36,.15);}
      #titSite, #titPage {display:inline-block;padding-left:20px;line-height:40px;text-transform:uppercase;color:#242424;font-size:12px;font-family:Raleway, verdana;transition:opacity 0.3s ease;position:relative;}
      #titSite {font-weight:700;z-index:6;margin-right:150px;}
      #titSite a {color:inherit;}
      .veloBG {position:fixed;background-color:#FFFFFF;width:100%;height:100%;left:0;top:0;z-index:-5000;opacity:0.93;-ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=93)";transition:all 0.7s ease;}
      #wrapperFoot {margin:80px auto 0 auto;padding:0 20px;}
      #linksFoot {display:inline-block;margin-left:50px;margin-right:20px;height:40px;line-height:40px;float:right;color:#242424;}#linksFoot a {color:inherit !important;opacity:0.7 !important;text-transform:lowercase;}
      #idiomas {padding-top:0;}
      #textFoot {text-align:right;font-size:0.88em;opacity:0.7;}@media only screen and (max-width:670px) {
        #linksFoot {position:relative;}
        #textFoot {margin-right:20px;text-align:right;width:100%;}
        #wrapperFoot {margin-top:80px;position:relative;}}
        .galeriaSBG #textFoot {display:none;}@media only screen and (min-width:80em) {
          .galeriaSBG .introPosicionTop, .galeriaSBG .introPosicionBottom {-webkit-column-count:2;-moz-column-count:2;column-count:2;-webkit-column-gap:2em;-moz-column-gap:2em;column-gap:2em;}
          .galeriaSBG .introPosicionTop, .galeriaSBG .introPosicionBottom {padding-right:2em;}}
          .subtitulo_g .thumbSBG .titulo {padding:0;bottom:-40px;height:38px;line-height:38px;text-align:center;border-bottom:none;background-color:#FFFFFF;background-color:rgba(255,255,255,.5);overflow:hidden;}.portadaFull ul.l2 {display:none;}
          .portadaFull.portadaSlogan .wrapperBanda.hideOnPF {display:block;background-color:transparent;}
          .portadaFull.portadaSlogan .lineTitulos {display:block;border-color:#FFF;}
          .portadaFull.portadaSlogan .wrapperBanda.hideOnPF #menu {display:inline-block;}
          .portadaFull.portadaSlogan .wrapperBanda.hideOnPF #menu li,.portadaFull.portadaSlogan .wrapperBanda.hideOnPF #titSite {color:#FFF;font-weight:normal;}
          .portadaFull.portadaSlogan .wrapperBanda.hideOnPF #titSite a {cursor:default;}
          .portadaFull.portadaSlogan .tiradorMenuPortadaF, .portadaFull.portadaSlogan .logoPortadaF, .portadaFull.portadaSlogan .menuPortadaF {display:none;}@media screen and (max-width:1024px) {
            .portadaFull.portadaSlogan .wrapperBanda.hideOnPF {display:block !important;margin-top:20px;margin-left:20px;}
            .portadaFull.portadaSlogan .wrapperBanda.hideOnPF #menu {display:none;}}
            .bodyBKblog #container {padding:0 0 50px;}
            .stripThumbs {opacity:0;}a {color:#474747;text-decoration:none;}
            .bk-oldie .galbk-bot-base.wfull-spread.prev {left:2%;font-size:20px;}@media screen and (max-width:48em) {
              .bk-touch body { font-size:21px } }
              .galeria_101497 .thumbSBG { border:none; background:#eeeeee; background:rgba(238,238,238, 0.64); border-radius:0px;}
              .encabezando{
                padding-top: 10px;
                background-color: red;
              }
            </style>
            <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
            <script type="text/javascript">var uriDomain = 'https://ac.bluekea.com/demo-duhr/';var uriUserSSL = 'https://ac.bluekea.com/demo-duhr/';var uriUserSSLlang = 'https://ac.bluekea.com/demo-duhr/';var uriBase = 'https://res.bluekea.com/account/';var uriBaseCDN = 'https://d3l48pmeh9oyts.cloudfront.net/';var langBK = '';var __codePortada = 'inicio';var uriCurrent = 'https://ac.bluekea.com/demo-duhr/';var uriSChar = false;</script>
            <script type="text/javascript" src="https://d3l48pmeh9oyts.cloudfront.net/min2/a_js__path__default/197"></script>

            <script type="text/javascript">
              $(function() { menuSBG = $('#menu').sbgMenu({cssMenu:'',colorAct:'#B0B0B0', colorNor:'#242424', colorHov:'#B0B0B0', primerCode:'inicio', blog: '0'}); })
            </script>
            <script type="text/javascript">

              function ajaxLoadPagina(_code) 
              {

                $('html').addClass('bk-loading-ajax');

                _code = typeof _code == 'undefined' ? '' : _code;

                $.ajax({
                  url: 'https://ac.bluekea.com/demo-duhr/index.php?ajax=1&c=' + _code,
                  dataType: 'json',
                  success: function(data, textStatus) {
                    $(document).unbind("keydown.key-galbk");
                    $('#controles').html('').hide();
                    $('#caption, .galbk-elm').html('').hide();
                    $('.compartirFoto').remove();
                    setTitulo(data.tituloSEO);
                    setCanonical(data.canonical);
                    stopSpinner();
                    $('body').removeClass().addClass(data.addClasses);
                    $('.idiomasBK').html(data.idiomas);
                    loadTituloPagina(data.targetTitulo, data.titulo);
                    $('#' + data.targetContenidos).html(data.contenidos);
                    $('#js_contents').html(data.contenidosJS);
                    codigoJS();
                    resumeMusic();
                    sendAlert();

                  },
                  error: function(xhr, textStatus, errorThrown) {}
                });
              }


            var avisoCookBK = false;</script>
            <meta property="og:url" content=""/>
            <meta property="og:image" content=""/>
            <meta property="og:type" content="website" />
            <meta property="og:image:width" content="1000" /> 
            <meta property="og:image:height" content="1127" />
          </head>
          <body class="inGal fotogrande portada intro paseFondo portadaMulti">
            <a class="tiradorCart" href="#" onclick="showCartBK();return false;">
              <svg class="">
                <use xlink:href="#i-shopping-cart" /></svg>
              </a>
              <div id="wrapperCartBK">
              </div>
              <div class="wrapperMobile hideOnPF">
                <div class="headerMobile">
                  <div class="logo">
                    <div class="logoT">Auge<span class="subTitulo">Punto Central</span>
                    </div>
                  </div>
                  <div class="la-ball-clip-rotate loader-bk">
                    <div>
                    </div>
                  </div>
                  <div class="titPagMobile fusibleMobile">Inicio</div>
                  <div class="linkMobile linkMenuMobile fusibleMobile">
                    <svg class="iconbars ">
                      <use xlink:href="#i-bars-btm" />
                    </svg>
                  </div>
                </div>
                <div class="menuMobile">
                  <svg class="">
                    <use xlink:href="#i-arrow-right closeMenuMobile" />
                  </svg>
                  <ul class="l1">
                  </ul>
                  <div class="footMobile">
                    <div class="siguenosFA ">
                      <a target="_blank" title="Facebook" href="https://facebook.com" class="siguenos_fb">
                        <svg class="">
                          <use xlink:href="#i-facebook" /></svg>
                        </a>
                        <a target="_blank" title="Twitter" href="https://twitter.com" class="siguenos_tw">
                          <svg class="">
                            <use xlink:href="#i-twitter" /></svg>
                          </a>
                          <a target="_blank" title="Instagram" href="https://instagram.com" class="siguenos_in">
                            <svg class="">
                              <use xlink:href="#i-instagram" /></svg>
                            </a>
                            <a title="Info" class="info-copyright" href="#" onclick="toggleInfoPie(); return false;">
                              <svg class="">
                                <use xlink:href="#i-copyright" />
                              </svg>
                            </a>
                          </div>
                        </div>
                      </div>
                      <div class="lineMobile fusibleMobile">
                      </div>
                    </div>
                    <div class="veloMenuMobile">
                    </div>
                    <div id="innerBody">

                      <div class="lineTitulos hide-mobile hideOnPF"></div>
                      <div id="container" class="fusible wsetter">

                        <div id="contents">

                          <div id="sbg_contents"><div class="wrapper-portada multi">
                            <div class="page-one banner">
                              <div class="logo" style="text-align: center;">
                                <img src="img/logo/ico.png" width="5%" >
                              </div>
                              <div class="logoSimple">
                                <span class="helperVA">

                                </span>
                              </a>
                            </div>
                            <div class="pase-fondo-container sfull-banner">
                              <div class="velo">

                              </div>
                            </div>
                            <a href="#" onclick="$('.wrapper-portada.multi').scrollTo('.seccion:first', 500); return false;"><div class="tirador-down">
                              <svg class="">
                                <use xlink:href="#i-long-arrow-down" /></svg>
                              </div>
                            </a>
                          </div>
                          <div class="seccion portada-mmenu">
                            <ul class="l1">

                            </ul></div><div class="intro-multi seccion"><div class="intro-portada"><p style="text-align:center;">
                              (Esto solo es un ejemplo.)<br> 
                              Oprah Winfrey nació en Misisipi el 29 de enero de 1954. Se formó como periodista y presentadora de televisión; además de ser crítica de libros. Fue varias veces ganadora del Premio Emmy por su programa The Oprah Winfrey Show, el programa de entrevistas más visto en la historia de la televisión. ​ Además es una influyente crítica de libros, actriz nominada a un Premio Óscar y editora de su propia revista. Según la revista Forbes, fue la persona afroamericana más rica del siglo XX y la única de origen negro en el mundo, en poseer más de mil millones de dólares durante tres años consecutivos. También se dice que fue la mujer más poderosa del año 2005, según Forbes.

                            </p>
                          </div></div><div class="galerias-portada seccion"><div class="tit-seccion-portada">Galerias</div>
                          <div id="galeria_101494" class="indiceGalerias galeria_101494 continua_g continua_ef_efecto1  pure-g 3">
                            <div class="pure-u-1-2 pure-u-sm-1-3">
                              <div class="thumb_bk">
                                <div class="thumb_placeholder" style="padding-bottom: 100%">
                                  <a href="">
                                    <img alt="Estudio y retrato" src="img/1.jpg"/>
                                    <div class="shieldTh"></div>
                                    <div class="veloTitulo"></div>
                                    <div class="titulo">Estudio y retrato</div>
                                  </a>
                                </div>
                              </div>
                            </div>

                            <div class="pure-u-1-2 pure-u-sm-1-3">
                              <div class="thumb_bk" ><div class="thumb_placeholder" style="padding-bottom: 100%;">
                                <a href="">
                                  <img alt="Figuras en el paisaje" src="img/2.jpg" />
                                  <div class="shieldTh"></div>
                                  <div class="veloTitulo"></div>
                                  <div class="titulo">Este es el 2</div>
                                </a>
                              </div>
                            </div>
                          </div>

                          <div class="pure-u-1-2 pure-u-sm-1-3">
                            <div class="thumb_bk">
                              <div class="thumb_placeholder" style="padding-bottom: 100%">
                                <a href="">
                                  <img alt="Blanco y negro" src="img/3.jpg"/><div class="shieldTh"></div>
                                  <div class="veloTitulo"></div>
                                  <div class="titulo">Blanco y negro</div>
                                </a>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="footer-multi">
                        <div class="siguenosFA ">
                          <a target="_blank" title="Facebook" href="https://facebook.com" class="siguenos_fb">
                            <svg class="">
                              <use xlink:href="#i-facebook" />
                            </svg>
                          </a>
                          <a target="_blank" title="Twitter" href="https://twitter.com" class="siguenos_tw">
                            <svg class="">
                              <use xlink:href="#i-twitter" /></svg>
                            </a>
                            <a target="_blank" title="Instagram" href="https://instagram.com" class="siguenos_in">
                              <svg class="">
                                <use xlink:href="#i-instagram" />
                              </svg>
                            </a><a title="Info" class="info-copyright" href="#" onclick="toggleInfoPie(); return false;">
                              <svg class="">
                                <use xlink:href="#i-copyright" /></svg>
                              </a>
                            </div>
                          </div>
                        </div>
                        <div class="img_pase_fondo" data-bk-imgfondo="http://augepuntocentral.com/img/columna/515/38dfaacd911796931af6f6beb72b8eff.jpg" style=""></div>
                        <div class="img_pase_fondo-m" data-bk-imgfondo-m="http://augepuntocentral.com/img/columna/515/38dfaacd911796931af6f6beb72b8eff.jpg"></div>
                        <div class="img_pase_fondo" data-bk-imgfondo="http://augepuntocentral.com/img/columna/514/31c0b57f511539c95fa79005db64fed3.jpg"></div>
                        <div class="img_pase_fondo-m" data-bk-imgfondo-m="http://augepuntocentral.com/img/columna/514/31c0b57f511539c95fa79005db64fed3.jpg"></div>
                      </div>
                      <div id="js_contents">

                      </div>
                      <div id="caption">

                      </div>
                    </div>
                    <br class="cleaner"/>
                  </div>

                  <div id="wrapperFoot" class="fusible reset-mobile wsetter">
                    <div id="textFoot"></div>
                  </div>

                  <div id="controles" class="controlesH"></div>

                  <div class="veloBG hideOnPF"></div><div id="copyright">&copy; Auge</div>
                  <script type="text/javascript">var codeGA = ''</script>
                  asddddddddd
                </div>


                <script>
                  function sbgIsHDdevice() {
                    var mediaQuery = "(-webkit-min-device-pixel-ratio: 1.5),\
                    (min--moz-device-pixel-ratio: 1.5),\
                    (-o-min-device-pixel-ratio: 3/2),\
                    (min-resolution: 1.5dppx)";
                    if (window.devicePixelRatio > 1)
                      return true;
                    if (window.matchMedia && window.matchMedia(mediaQuery).matches)
                      return true;
                    return false;
                  }
                  if (sbgIsHDdevice()) {
                    document.cookie="bluekea_hddevice=ok";
                  }
                </script>


                <script>
                  $(function() {
                    fadeInContent();;;praiseRotator();imgFondoPase();checkLogoHDportadaFull();portada();;$('.botonesMusic').show();;makeIframeResponsive('#sbg_contents', '');;activarMenuComun();menuSBG.loadTitulo('Inicio'); })
                  sendAlert();
                </script>
                <svg width="0" height="0" class="oculto-pos">
                  <symbol id="i-angle-left-btm" viewBox="0 0 576 1008"><g transform="matrix(1 0 0 -1 0 864)">
                   <path d="M18 360l504 504l54 -54l-450 -450l450 -450l-54 -54z"/>
                 </g></symbol><symbol id="i-angle-right-btm" viewBox="0 0 576 1008"><g transform="matrix(1 0 0 -1 0 864)">
                   <path d="M54 864l504 -504l-504 -504l-54 54l450 450l-450 450z"/>
                 </g></symbol><symbol id="i-arrow-right" viewBox="0 0 1008 1008"><g transform="matrix(1 0 0 -1 0 840)">
                   <path d="M167 294v82h504l-230 233l59 59l333 -333l-333 -333l-59 59l230 233h-504z"/>
                 </g></symbol><symbol id="i-bars" viewBox="0 -50 864 1008"><path d="M864 684v-72c0 -20 -16 -36 -36 -36h-792c-20 0 -36 16 -36 36v72c0 20 16 36 36 36h792c20 0 36 -16 36 -36zM864 396v-72c0 -20 -16 -36 -36 -36h-792c-20 0 -36 16 -36 36v72c0 20 16 36 36 36h792c20 0 36 -16 36 -36zM864 108v-72c0 -20 -16 -36 -36 -36h-792 c-20 0 -36 16 -36 36v72c0 20 16 36 36 36h792c20 0 36 -16 36 -36z"/></symbol><symbol id="i-bars-btm" viewBox="0 0 1008 1008"><g transform="matrix(1 0 0 -1 0 864)">
                   <path d="M1008 432v-72h-1008v72h1008zM1008 72v-72h-1008v72h1008zM1008 792v-72h-1008v72h1008z"/>
                 </g></symbol><symbol id="i-circle-o" viewBox="0 0 864 1008"><g transform="matrix(1 0 0 -1 0 864)">
                   <path d="M864 360c0 -238 -193 -432 -432 -432s-432 194 -432 432s193 432 432 432v0c239 0 432 -194 432 -432zM432 666c-169 0 -306 -137 -306 -306s137 -306 306 -306s306 137 306 306s-137 306 -306 306z"/>
                 </g></symbol><symbol id="i-copyright" viewBox="0 0 864 1008"><g transform="matrix(1 0 0 -1 0 864)">
                   <path d="M864 360c0 -238 -193 -432 -432 -432s-432 194 -432 432s193 432 432 432v0c239 0 432 -194 432 -432zM432 720c-199 0 -360 -161 -360 -360s161 -360 360 -360s360 161 360 360s-161 360 -360 360zM647 260v-61c0 -80 -127 -109 -206 -109c-154 0 -270 118 -270 273 c0 152 115 267 267 267c56 0 202 -20 202 -109v-61c0 -6 -4 -9 -9 -9h-67c-5 0 -9 4 -9 9v39c0 35 -68 52 -114 52c-105 0 -178 -76 -178 -185c0 -113 76 -196 182 -196c41 0 118 15 118 51v39c0 5 3 9 8 9h67c4 0 9 -4 9 -9z"/>
                 </g></symbol><symbol id="i-envelope" viewBox="0 0 1008 1008"><g transform="matrix(1 0 0 -1 0 864)">
                   <path d="M1008 630c0 -63 -47 -120 -96 -154c-88 -61 -176 -122 -263 -183c-37 -25 -99 -77 -144 -77h-1h-1c-45 0 -107 52 -144 77c-87 61 -175 123 -262 183c-40 27 -97 90 -97 142c0 55 30 102 90 102h828c49 0 90 -40 90 -90zM1008 465v-447c0 -50 -40 -90 -90 -90h-828 c-50 0 -90 40 -90 90v447c17 -19 36 -35 57 -49c93 -64 188 -127 279 -194c48 -35 106 -78 167 -78h1h1c61 0 119 43 167 78c91 66 186 130 280 194c20 14 39 30 56 49z"/>
                 </g></symbol><symbol id="i-facebook" viewBox="0 0 864 1008"><g transform="matrix(1 0 0 -1 0 864)">
                   <path d="M162 792h540c89 0 162 -73 162 -162v-540c0 -89 -73 -162 -162 -162h-106v335h112l17 130h-129v83c0 38 10 63 65 63l69 1v116c-12 2 -53 6 -101 6c-99 0 -168 -61 -168 -173v-96h-112v-130h112v-335h-299c-89 0 -162 73 -162 162v540c0 89 73 162 162 162z"/>
                 </g></symbol><symbol id="i-instagram" viewBox="0 0 864 1008"><g transform="matrix(1 0 0 -1 0 864)">
                   <path d="M864 360c0 -60 0 -119 -3 -178c-3 -69 -19 -131 -70 -181c-50 -51 -112 -67 -181 -70c-59 -3 -118 -3 -178 -3s-119 0 -178 3c-69 3 -131 19 -181 70c-51 50 -67 112 -70 181c-3 59 -3 118 -3 178s0 119 3 178c3 69 19 131 70 181c50 51 112 67 181 70c59 3 118 3 178 3 s119 0 178 -3c69 -3 131 -19 181 -70c51 -50 67 -112 70 -181c3 -59 3 -118 3 -178zM432 714c-63 0 -198 5 -255 -17c-20 -8 -34 -18 -49 -33s-25 -29 -33 -49c-22 -57 -17 -192 -17 -255s-5 -198 17 -255c8 -20 18 -34 33 -49s29 -25 49 -33c57 -22 192 -17 255 -17 s198 -5 255 17c20 8 34 18 49 33c16 15 25 29 33 49c22 57 17 192 17 255s5 198 -17 255c-8 20 -17 34 -33 49c-15 15 -29 25 -49 33c-57 22 -192 17 -255 17zM714 591c0 -29 -23 -52 -51 -52c-29 0 -52 23 -52 52c0 28 23 51 52 51c28 0 51 -23 51 -51zM654 360 c0 -123 -99 -222 -222 -222s-222 99 -222 222s99 222 222 222s222 -99 222 -222zM576 360c0 79 -65 144 -144 144s-144 -65 -144 -144s65 -144 144 -144s144 65 144 144z"/>
                 </g></symbol><symbol id="i-long-arrow-down" viewBox="0 0 576 1008"><g transform="matrix(1 0 0 -1 0 864)">
                   <path d="M288 -144l-252 252l54 54l162 -162v864h72v-864l162 162l54 -54z"/>
                 </g></symbol><symbol id="i-long-arrow-up" viewBox="0 0 576 1008"><g transform="matrix(1 0 0 -1 0 864)">
                   <path d="M36 612l252 252l252 -252l-54 -54l-162 162v-864h-72v864l-162 -162z"/>
                 </g></symbol><symbol id="i-menu" viewBox="0 0 1008 1008"><g transform="matrix(1 0 0 -1 0 819)">
                   <path d="M1008 567h-1008v126h1008v-126zM1008 193h-1008v126h1008v-126zM1008 -185h-1008v126h1008v-126z"/>
                 </g></symbol><symbol id="i-pause" viewBox="0 0 576 1008"><g transform="matrix(1 0 0 -1 0 864)">
                   <path d="M576 864v-1008h-144v1008h144zM144 864v-1008h-144v1008h144z"/>
                 </g></symbol><symbol id="i-play" viewBox="0 0 792 1008"><g transform="matrix(1 0 0 -1 0 864)">
                   <path d="M779 343l-747 -416c-18 -9 -32 -1 -32 19v828c0 20 14 28 32 19l747 -416c17 -9 17 -25 0 -34z"/>
                 </g></symbol><symbol id="i-quote-entypo" viewBox="0 0 1008 1008"><g transform="matrix(1 0 0 -1 0 864)">
                   <path d="M692 690l1 -3c85 0 143 -38 174 -115c29 -71 30 -155 2 -253s-76 -175 -144 -231c-55 -45 -115 -67 -180 -67v71c66 0 122 29 167 87c36 45 53 89 51 132c-2 49 -26 73 -71 73c-41 0 -76 15 -105 45s-43 66 -43 108s14 78 43 108s64 45 105 45zM268 693l1 -3 c85 0 143 -38 174 -115c29 -71 30 -155 2 -253s-76 -175 -144 -231c-55 -45 -115 -67 -180 -67v71c66 0 122 29 167 87c36 45 53 89 51 132c-2 49 -26 73 -71 73c-41 0 -76 15 -105 45s-43 66 -43 108s14 78 43 108s64 45 105 45z"/>
                 </g></symbol><symbol id="i-times-btm" viewBox="0 0 1008 1008"><g transform="matrix(1 0 0 -1 0 864)">
                   <path d="M1008 810l-450 -450l450 -450l-54 -54l-450 450l-450 -450l-54 54l450 450l-450 450l54 54l450 -450l450 450z"/>
                 </g></symbol><symbol id="i-twitter" viewBox="0 0 936 1008"><g transform="matrix(1 0 0 -1 0 864)">
                   <path d="M911 634c-25 -35 -55 -68 -91 -93c1 -8 1 -16 1 -24c0 -240 -183 -517 -517 -517c-103 0 -199 30 -279 82c14 -2 28 -3 44 -3c85 0 163 29 225 78c-80 2 -147 54 -170 126c12 -2 23 -3 35 -3c16 0 32 2 47 6c-83 17 -145 90 -145 179v2c24 -14 52 -22 82 -23 c-49 32 -81 88 -81 151c0 34 9 65 25 92c89 -110 224 -182 374 -190c-3 13 -4 27 -4 41c0 100 81 182 181 182c53 0 100 -22 133 -57c41 8 81 23 115 44c-13 -43 -42 -78 -79 -101c36 4 72 14 104 28z"/>
                 </g></symbol></svg></body>
                 </html>